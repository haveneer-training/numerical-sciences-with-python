import random


def retry(func):
    def retried_function(*args, **kwargs):
        exc = None
        for _ in range(4):
            try:
                return func(*args, **kwargs)
            except Exception as local_exception:
                print("Exception raised [%s] while calling %s with args:%s, kwargs: %s. Retrying"
                      % (local_exception, func, args, kwargs))
                exc = local_exception
        raise exc

    return retried_function


def do_something_risky():
    if random.choice([True, False]):
        raise Exception("exception occurs")
    else:
        return 1


if __name__ == '__main__':
    retried_function = retry(do_something_risky)  # No need to use `@`

    random.seed()
    print(retried_function())
