#!/usr/bin/env python

import sys

if len(sys.argv) != 3:
    raise Exception("Bad argument count")


def extract(s):
    return s.strip().split(',')[0]


with open(sys.argv[1], 'r') as file:
    lines = file.readlines()
    dico = set([line.strip() for line in lines])


def f(s):
    if s + 'ly' in dico:
        return True
    else:
        return False


with open(sys.argv[2], 'r') as file:
    lines = file.readlines()
    lines = [extract(line) for line in lines]
    matches = [s for s in lines if f(s)]

print(matches)
print(len(matches))
