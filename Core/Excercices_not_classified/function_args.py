def f(a, /, b, b0="b0", *, c, c0="c0"):
    return "".join([str(x) for x in (a, b, b0, c, c0)])
