def intersect(l1, l2):
    s2 = set(l2)
    return [x for x in l1 if x in s2]