import matplotlib.pyplot as plt

if __name__ == '__main__':
    x = [1, 2, 3, 4, 10]
    y = [2, 4, 5.5, 8.5, 19]
    y_pred = [2, 4, 6, 8, 20]

    plt.scatter(x, y, color="r", marker="o")
    plt.xlabel("X label")
    plt.ylabel("y label")
    plt.title("Titile")
    plt.plot(x, y_pred, color="b")
    plt.show()
