import numpy as np

# number of tests = 100
# 10 ≤ m ≤ 100
# 10 ≤ n ≤ 100
# -10000 ≤ m[i,j] ≤ 10000

def sort_matrix(m):
    r = np.sort(m, axis=1)
    r = np.sort(r, axis=0)
    return r


def verif_matrix(r):
    # can use : np.all(np.diff(a) >= 0)
    ok = True
    for i in range(r.shape[0]):
        ok &= np.all(r[i, :-1] <= r[i, 1:])

    for j in range(r.shape[1]):
        ok &= np.all(r[:-1, j] <= r[1:, j])
    return ok
