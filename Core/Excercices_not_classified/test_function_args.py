import pytest
from function_args import *


def test1():
    with pytest.raises(TypeError):
        f(1, 2)
    assert f(1, 2, c="c") == "12b0cc0"
    assert f(1, 2, "b0", c="c") == "12b0cc0"
    assert f(1, b=2, c0="c1", c="c") == "12b0cc1"
    assert f(1, b0="b0", b=2, c="c") == "12b0cc0"
