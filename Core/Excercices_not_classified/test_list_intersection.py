from list_intersection import *


def test1():
    assert intersect([2, 3, 5, 4], [5, 100, 2]) == [2, 5]
    assert intersect([5, 100, 2], [2, 3, 5, 4]) == [5, 2]
    assert intersect([2, 3, 5, 4], [3, 8, 9, 2, 10]) == [2, 3]
    assert intersect([3, 8, 9, 2, 10], [2, 3, 5, 4]) == [3, 2]
    assert intersect([5, 100, 2], [3, 8, 9, 2, 10]) == [2]
    assert intersect([3, 8, 9, 2, 10], [5, 100, 2]) == [2]
