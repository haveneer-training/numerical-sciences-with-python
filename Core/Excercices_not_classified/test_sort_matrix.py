import pytest
import numpy as np
from sort_matrix import *


def test_1():
    matrix = np.array([[7, 1, 4],
                       [3, 2, 8],
                       [6, -1, -6]])
    result = np.array([[-6, -1, 6],
                       [1, 3, 7],
                       [2, 4, 8]])
    assert np.array_equal(sort_matrix(matrix), result)
    assert verif_matrix(sort_matrix(matrix))


def test_2():
    matrix = np.array([[7, 1, 4, 2, -55],
                       [8, 2, -33, 1, 9],
                       [5, 61, -9, 3, 8],
                       [2, -12, 9, 51, 6],
                       [4, 7, 2, 2, 1]])
    result = np.array([[-55, 1, 2, 4, 7],
                       [-33, 1, 2, 4, 7],
                       [-12, 2, 2, 8, 9],
                       [-9, 2, 5, 8, 51],
                       [1, 3, 6, 9, 61]])
    assert np.array_equal(sort_matrix(matrix), result)
    assert verif_matrix(sort_matrix(matrix))


def test_3():
    matrix = np.array([[5, 5, 5],
                       [5, 5, 5],
                       [5, 5, 5]])
    result = np.array([[5, 5, 5],
                       [5, 5, 5],
                       [5, 5, 5]])
    assert np.array_equal(sort_matrix(matrix), result)
    assert verif_matrix(sort_matrix(matrix))


def test_4():
    matrix = np.array([[5, 5, 5],
                       [5, 5, 5],
                       [5, 5, 5]])
    result = sort_matrix(matrix)
    assert np.array_equal(result, matrix)
    assert verif_matrix(sort_matrix(matrix))
    matrix[1, 1] = 0
    assert not np.array_equal(result, matrix)


def test_5():
    import random
    for _ in range(100):
        n = random.randint(10, 100)
        m = random.randint(10, 100)
        matrix = np.random.randint(20000, size=(n, m)) - 10000
        result = sort_matrix(matrix)
        assert verif_matrix(result)


def test_6():
    assert not verif_matrix(np.array([[5, 2], [3, 4]]))
    assert not verif_matrix(np.array([[4, 5, 6], [1, 2, 3]]))
    assert not verif_matrix(np.array([[4, 1], [5, 2]]))
