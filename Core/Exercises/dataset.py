import csv
from matrix import Matrix
from vector import Vector
import stat_tools


class Dataset:
    def __init__(self, filename):
        with open(filename, newline='') as csvfile:
            reader = csv.reader(csvfile)
            self.columns = [c.strip() for c in next(reader)][1:]
            self.index = []
            data = []
            for line in reader:
                self.index.append(line.pop(0).strip())
                data.append([float(c) for c in line])
            self.data = Matrix(data)

    def __getitem__(self, item):
        c = self.columns.index(item)
        return Vector([self.data[(i, c)] for i in range(self.data.shape[0])])

    def describe(self):
        column_length = [len(column) for column in self.columns]
        title = ""
        print(f"{title:10}", end='')
        for (i, c) in enumerate(self.columns):
            print(f"{c:{column_length[i]}} ", end='')
        print()
        title = "mean:"
        print(f"{title:10}", end='')
        for (i, c) in enumerate(self.columns):
            print(f"{stat_tools.mean(self[c].values):{column_length[i]}.2} ", end='')
        print()
        title = "median:"
        print(f"{title:10}", end='')
        for (i, c) in enumerate(self.columns):
            print(f"{stat_tools.median(self[c].values):{column_length[i]}.2} ", end='')
        print()
        title = "std:"
        print(f"{title:10}", end='')
        for (i, c) in enumerate(self.columns):
            print(f"{stat_tools.std(self[c].values):{column_length[i]}.2} ", end='')
        print()

    def __iter__(self):
        return SeriesIterator(self)


class SeriesIterator:
    def __init__(self, dataset):
        self.__dataset = dataset
        self.__iter = iter(dataset.columns)

    def __iter__(self):
        return self

    def __next__(self):
        title = next(self.__iter)
        return SeriesValue(title, self.__dataset[title])


class SeriesValue:
    def __init__(self, title, values):
        self.title = title
        self.values = values


if __name__ == '__main__':
    d = Dataset("data.csv")  # Retourne une exception ValueError si les contraintes ne sont pas respectées
    assert d.columns == ['Margarine consumed (lbs)', 'Divorce rate in Maine (/1000)']
    assert d.index == ['2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009']
    print(d.columns)  # affiche: ['Margarine consumed (lbs)', 'Divorce rate in Maine (/1000)']
    assert d.columns == ['Margarine consumed (lbs)', 'Divorce rate in Maine (/1000)']
    print(d.index)  # affiche: ['2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009']
    assert d.index == ['2000', '2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009']
    print(d[d.columns[0]])  # affiche: Vector([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
    assert d[d.columns[0]].values == [8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7]

    d.describe()  # affiche: TODO
    #           Margarine consumed (lbs) Divorce rate in Maine (/1000)
    # mean:                          5.3                           4.4
    # median:                        4.9                           4.2
    # std:                           1.4                          0.28

    for column_iter in d:
        print(column_iter.title)
        print(f"\t{column_iter.values}")

    column_iter = iter(d)
    column = next(column_iter)
    assert column.title == "Margarine consumed (lbs)"
    assert column.values.values == [8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7]
    column = next(column_iter)
    assert column.title == "Divorce rate in Maine (/1000)"
    assert column.values.values == [5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1]
