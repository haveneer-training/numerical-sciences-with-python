def extract_values_in_range(data, minv, maxv):
    try:
        return [x for x in data if minv < x < maxv]
    except:
        return None


