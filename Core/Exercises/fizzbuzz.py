def fizzbuzz1(n1, n2):
    def inner_fizzbuzz(x):
        fizz = not x % 3
        buzz = not x % 5

        if fizz and buzz:
            return "FizzBuzz"
        elif fizz:
            return "Fizz"
        elif buzz:
            return "Buzz"
        else:
            return str(x)

    return "\n".join([inner_fizzbuzz(x) for x in range(n1, n2 + 1)])


def fizzbuzz2(n1, n2):
    return '\n'.join('Fizz' * (not i % 3) + 'Buzz' * (not i % 5) or str(i)
                     for i in range(n1, n2 + 1))


# choix de la variante
fizzbuzz = fizzbuzz1

if __name__ == '__main__':
    assert fizzbuzz1(1, 2000) == fizzbuzz2(1, 2000)
    print(fizzbuzz(1, 200))
