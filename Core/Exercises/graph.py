class DuplicateNodeError(Exception):
    pass


class MissingNodeError(Exception):
    pass


class Tree:
    class Node:
        def __init__(self, label, parent, tree, weight=None):
            self.parent = parent
            self.weight = weight
            self.label = label
            self.children = []
            self.tree = tree

        def add_node(self, label, weight):
            if not all(node.label != label for node in self.children):
                raise DuplicateNodeError()
            if label in self.tree.labels:
                raise DuplicateNodeError()
            new_node = Tree.Node(label, self, self.tree, weight)
            self.tree.labels[label] = new_node
            self.children.append(new_node)
            return new_node

        def _cascade_nodes(self):
            node_list = []
            for node in self.children:
                node_list.append(f"({self.label}->{node.label})")
                node_list.extend(node._cascade_nodes())
            return node_list

        def path_length_to_parent(self, parent_node):
            length = 0
            current_node = self
            while current_node != parent_node and current_node is not None:
                if current_node.parent is not None:
                    length += current_node.weight
                current_node = current_node.parent
            if current_node == parent_node:
                return length
            else:
                return None

    def __init__(self):
        self.labels = dict()  # name => node
        self.root = None

    def add_node(self, label):
        if self.root is not None:
            raise DuplicateNodeError()
        if label in self.labels:
            raise DuplicateNodeError()
        self.root = self.Node(label, None, self)
        self.labels[label] = self.root
        return self.root

    def __str__(self):
        return ", ".join(self.root._cascade_nodes())

    def __getitem__(self, label):
        if label not in self.labels:
            raise MissingNodeError()
        else:
            return self.labels[label]

    def path_length(self, first_label, second_label):
        first_node = self[first_label]
        second_node = self[second_label]
        return second_node.path_length_to_parent(first_node)
