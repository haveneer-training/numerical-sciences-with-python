class Animal:
    def __init__(self, name):
        self.__name = name

    name = property(lambda self: self.__name, None)


class Dog(Animal):
    def __init__(self, name):
        super().__init__(name)

    def wafwaf(self):
        print("WafWaf")


class Cat(Animal):
    def __init__(self, name):
        super().__init__(name)

    def miaou(self):
        print("Miaou")
