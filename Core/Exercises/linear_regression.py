from dataset import Dataset
from stat_tools import *

import matplotlib.pyplot as plt


def compute_regression_coefficients(x, y):
    mean_x, mean_y = mean(x.values), mean(y.values)

    # calculating cross-deviation and deviation about x
    SS_xy = sum([yi * xi - mean_y * mean_x for xi, yi in zip(x.values, y.values)])
    SS_xx = sum([xi * xi - mean_x * mean_x for xi in x.values])

    # compute regression coefficients
    # cf https://en.wikipedia.org/wiki/Simple_linear_regression
    beta = SS_xy / SS_xx
    alpha = mean_y - beta * mean_x

    return alpha, beta


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = "data.csv"

    d = Dataset(filename)
    x, y = d[d.columns[0]], d[d.columns[1]]

    # compute coefficients
    b = compute_regression_coefficients(x, y)
    print("Estimated coefficients:\nalpha = {} \nbeta = {}".format(b[0], b[1]))

    # plotting original data
    plt.scatter(x.values, y.values, color="r", marker="o")
    plt.xlabel(d.columns[0])
    plt.ylabel(d.columns[1])

    # compute predicted response
    y_pred = [b[0] + b[1] * xi for xi in x.values]

    # plotting the regression line
    plt.plot(x.values, y_pred, color="b")

    # computing the coefficient of determination (R2)
    # cf https://en.wikipedia.org/wiki/Coefficient_of_determination
    SS_res = sum([(yi - yi_pred) ** 2 for (yi, yi_pred) in zip(y.values, y_pred)])
    mean_y = mean(y.values)
    SS_tot = sum([(yi - mean_y) ** 2 for yi in y.values])
    R2 = 1 - SS_res / SS_tot

    print(f"R^2 = {R2}")

    plt.title(f"Linear regression: y = {b[0]:.3} + {b[1]:.3} * x ; R^2 = {R2:.3}")

    plt.show()
