import math
from vector import Vector
import copy


# La classe d'exception spécifique pour les problèmes de forme
class MalformedDataError(Exception):
    """Exception for malformed matrix data"""
    pass


# Par rapport à Vector ici les erreurs sont principalement gérées comme des erreurs natives Python
# sans adaptation spécifique pour la classe Matrix
class Matrix:
    def __init__(self, values):
        if len(values) == 0:
            raise MalformedDataError("Empty values")
        else:
            n = len(values[0])
            if any([len(v) != n for v in values]):
                raise MalformedDataError("Bad subsizes")
        self.__values = copy.deepcopy(values)  # il faut employer deepcopy quand nous avons des listes imbriquées
        self.__shape = (len(self.__values), len(self.__values[0]))

    def __mul__(self, other):
        if isinstance(other, Matrix):
            if self.__shape[1] != other.__shape[0]:
                raise ValueError("Incompatible matrix * matrix size")
            m = Matrix([[0] * other.__shape[1] for i in range(self.__shape[0])])
            for i in range(self.__shape[0]):
                for j in range(other.__shape[1]):
                    r = 0
                    for k in range(self.__shape[1]):
                        r += self[(i, k)] * other[(k, j)]
                    m[(i, j)] = r
            return m
        elif isinstance(other, Vector):
            if self.__shape[1] != other.size:
                raise ValueError("Incompatible matrix * vector size")
            v = Vector([0] * self.__shape[0])
            for i in range(self.__shape[0]):
                r = 0
                for j in range(other.size):
                    r += self[(i, j)] * other.values[j]
                v[i] = r
            return v
        else:
            raise TypeError

    def __getitem__(self, item):
        return self.__values[item[0]][item[1]]

    def __setitem__(self, item, value):
        self.__values[item[0]][item[1]] = value

    def __repr__(self):
        return f"Matrix([{', '.join(str(x) for x in self.__values)}])"

    def __str__(self):
        return f"Matrix([{', '.join(str(x) for x in self.__values)}])"

    def __get_shape(self):
        return self.__shape

    # la définition du None dans ces 'setters' des propriétés shape et values
    # empêche toute modification depuis l'extérieur de la classe.
    shape = property(__get_shape, None)
    values = property(lambda self: self.__values, None)  # version courte avec une lambda

    def diag(self):
        assert self.shape[0] == self.shape[1]
        return Vector([self.__values[i][i] for i in range(0, self.shape[0])])


def dot(self, other):
    if not isinstance(self, Vector) or not isinstance(other, Vector):
        raise TypeError
    return sum((xi * yi for xi, yi in zip(self.__values, other.values)))


if __name__ == '__main__':
    m1 = Matrix([[0.0, 1.0, 2.0, 3.0],  # m1.shape == (2,4)
                 [0.0, 2.0, 4.0, 6.0]])
    assert m1.shape == (2, 4)
    m2 = Matrix([[0.0, 1.0],  # m2.shape == (4,2)
                 [2.0, 3.0],
                 [4.0, 5.0],
                 [6.0, 7.0]])
    assert m2.shape == (4, 2)
    m3 = m1 * m2  # m3.values == [[28,34],[56,68]]
    assert m3.values == [[28, 34], [56, 68]]
    print(m3)  # affiche: Matrix([[28,34],[56,68]])
    assert str(m3) == "Matrix([[28.0, 34.0], [56.0, 68.0]])"
    v1 = (m2 * m1).diag()  # retourne la diagonale du produit entre m2 et m1
    print(v1)
    assert v1.values == [0, 8, 28, 60] and v1.size == 4
    v2 = m1 * v1
    assert v2.values == [244, 488] and v2.size == 2
    print(v2)  # affiche: Vector([...])

    # Sans la deepcopy de Matrix.__init__, les modifications des listes utilisées à la construction
    # sont directement répercutées sur les structures internes de la matrice.
    # Ainsi,
    # * la situation 1 changerait le résultat du produit
    # * la situation 2 donnerait des objets incompatibles en dimension

    # Test init by size
    try:
        Matrix(1)
    except TypeError as err:
        print("[OK]: value type check")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on value type check")
    else:
        print("[FAILED]: missing exception on value type check")

    try:
        Matrix([1, 2])
    except TypeError as err:
        print("[OK]: value type check")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on value type check")
    else:
        print("[FAILED]: missing exception on value type check")

    try:
        Matrix([[1], [1, 2]])
    except MalformedDataError as err:
        print("[OK]: dimension check")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on dimension check")
    else:
        print("[FAILED]: missing exception on dimension check")

    try:
        m1 = Matrix([[1, 2], [3, 4]])
        m2 = Matrix([[1]])
        m1 * m2
    except ValueError as err:
        print("[OK]: compatibility check")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on compatibility check")
    else:
        print("[FAILED]: missing exception on compatibility check")
