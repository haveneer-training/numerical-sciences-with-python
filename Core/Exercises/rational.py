class Rational:
    def __init__(self, p, q=1):
        if not isinstance(p, int) or not isinstance(q, int):
            raise InvalidNumberType()
        if q == 0:
            raise InvalidRationalOperation()
        pgcd = self.__pgcd(p, q)
        self.p = p // pgcd  # integer division required for subsequenced ops
        self.q = q // pgcd

    def __add__(self, other):
        if isinstance(other, Rational):
            return Rational(self.p * other.q + other.p * self.q, self.q * other.q)
        elif isinstance(other, int):
            return self + Rational(other)
        else:
            raise InvalidRationalOperation()

    def __radd__(self, other):
        return self + other

    def __sub__(self, other):
        return Rational(self.p * other.q - other.p * self.q, self.q * other.q)

    def __mul__(self, other):
        return Rational(self.p * other.p, self.q * other.q)

    def __truediv__(self, other):
        return Rational(self.p * other.q, self.q * other.p)

    def __str__(self):
        if self.q == 1:
            return f"{self.p}"
        else:
            return f"{self.p}/{self.q}"

    def __pgcd(self, a, b):
        if b == 0:
            return a
        else:
            r = a % b
            return self.__pgcd(b, r)


class InvalidNumberType(Exception):
    pass


class InvalidRationalOperation(Exception):
    pass
