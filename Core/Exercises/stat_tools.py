def mean(data):
    if not data:
        return None
    return sum(data) / len(data)


def median(data):
    if not data:
        return None
    sdata = sorted(data)
    n = len(data)
    if n % 2 == 0:
        return (sdata[n // 2] + sdata[n // 2 - 1]) / 2
    else:
        return sdata[n // 2]


def std(data):
    if not data:
        return None
    from math import sqrt
    m = mean(data)
    n = len(data)
    return sqrt(sum((x - m) ** 2 for x in data) / n)


if __name__ == '__main__':
    data1 = [1, 5, 2, 9, 0, 103]
    data2 = [1, 5, 2, 9, 0]
    data3 = None
    print(mean(data1))
    print(mean(data2))
    print(mean(data3))
    assert mean(data1) == 20
    assert mean(data2) == 3.4
    assert mean(data3) is None
    print(median(data1))
    print(median(data2))
    print(median(data3))
    assert median(data1) == 3.5
    assert median(data2) == 2
    assert median(data3) is None
    print(std(data1))
    print(std(data2))
    print(std(data3))
    assert std(data1) == 37.23797345005051
    assert std(data2) == 3.261901286060018
    assert std(data3) is None
