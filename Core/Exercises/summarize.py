import hashlib
import re


def sign_string(content):
    simplifier = re.compile(r"(\s+)|(#.*$)")
    local_hasher = hashlib.sha256()
    for line in content.splitlines():
        byte_array = bytes(simplifier.sub('', line), 'utf-8')  # encoding to 'utf-8'
        local_hasher.update(byte_array)
    return local_hasher.hexdigest()


def verify_signature(content):
    default_signature = "0" * 64
    signature = default_signature
    for line in content.splitlines():
        match = re.match(r"^\s*#\s*([0-9a-fA-F]{64})(\W.*)?$", line)
        if match:
            signature = match.group(1)
            break
        if line:  # stops after first not empty line
            print("La première ligne doit être une signature numérique")
            print("La signature numérique par défaut est:")
            print("#", "0" * 64)
            return False
    if signature != default_signature:
        if signature != sign_string(content):
            print("La signature ne correspond pas.")
            print("Mettez soit une signature produite par summarize.py,")
            print("soit la signature par défaut:")
            print("#", "0" * 64)
        else:
            return True
    else:
        return True


def sign_file(filename):
    with open(filename, newline='') as file:
        content = file.read()
        return sign_string(content)


def summarize(files, timestamp):
    hasher = hashlib.sha256()
    results = []
    hasher.update(bytes(timestamp, 'utf-8'))
    for filename in files:
        local_hash = sign_file(filename)
        results.append(f"# {local_hash} {filename}")
        hasher.update(bytes(local_hash, 'utf-8'))
    return hasher.hexdigest(), results


if __name__ == '__main__':
    import argparse
    from datetime import datetime

    parser = argparse.ArgumentParser(description='Process file integrity')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("file", metavar="FILE", type=str, nargs="?", help="File to check")
    group.add_argument("--check", action="store_true",
                       help="Verify signature of files in signature.sha256 [Default action]")
    group.add_argument("--sign", action="store_true",
                       help="Make a signature.sha256 file; this overwrite the previous one")
    args = parser.parse_args()

    if args.check:
        try:
            with open("signature.sha256", "r") as f:
                hasher = hashlib.sha256()
                lines = f.readlines()

                match = re.match(r"^\s*#\s*([0-9a-fA-F]{64})\s+(.*)?$", lines[-1])
                if match:
                    global_signature = match.group(1)
                    global_timestamp = match.group(2)
                    hasher.update(bytes(global_timestamp, 'utf-8'))
                else:
                    print(f"Error on final line: malformed line")

                for i, line in enumerate(lines[:-1]):
                    match = re.match(r"^\s*#\s*([0-9a-fA-F]{64})\s+(.*)?$", line)
                    if match:
                        signature = match.group(1)
                        filename = match.group(2)
                        if sign_file(filename) == signature:
                            print(f"[  OK  ] {filename}")
                            hasher.update(bytes(signature, 'utf-8'))
                        else:
                            print(f"[FAILED] {filename}")
                    else:
                        print(f"Error on line {i}: malformed line")

                if hasher.hexdigest() == global_signature:
                    print(f"[  OK  ] Timestamp: {global_timestamp}")
                else:
                    print(f"[FAILED] Timestamp: {global_timestamp}")
        except FileNotFoundError:
            print("Cannot open signature.sha256, create it before checking using --sign option")
            exit(1)
    elif args.file:
        print("#", sign_file(args.file), args.file)
    elif args.sign:
        files = ('extract_values_in_range.py',
                 'dataset.py',
                 'inheritance.py',
                 'vector.py',
                 'matrix.py',
                 'linear_regression.py',
                 'fizzbuzz.py',
                 'extract_values_in_range.py',
                 )
        timestamp = datetime.now()
        signature, results = summarize(files, str(timestamp))
        with open("signature.sha256", "w") as f:
            f.write("\n".join(results))
            f.write(f"\n# {signature} {timestamp}")
        with open("signature.sha256", "r") as f:
            print(f.read())
