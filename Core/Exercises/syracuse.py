def syracuse(n):
    if n < 1:
        return
    while True:
        yield n
        if n == 1:
            return
        if n % 2 == 0:
            n >>= 1
        else:
            n = 3 * n + 1


if __name__ == '__main__':
    for x in syracuse(10):
        print(x)
