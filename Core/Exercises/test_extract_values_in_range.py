import pytest

from extract_values_in_range import *


def test_data():
    data = [1, 2, 3, 4, 5, 6, -1, 5]
    minv = 2
    maxv = 6
    print(extract_values_in_range(data, minv, maxv))
    assert extract_values_in_range(data, minv, maxv) == [3, 4, 5, 5]


def test_none():
    data = [1, 2, 3, None, 5, 6, -1, 5]
    minv = 2
    maxv = 6
    print(extract_values_in_range(data, minv, maxv))
    assert extract_values_in_range(data, minv, maxv) is None


def test_data2():
    data = [1, 2.4, 3, -0.1, 5, 6, -1, 5]
    minv = 2
    maxv = 6.1
    print(extract_values_in_range(data, minv, maxv))
    assert extract_values_in_range(data, minv, maxv) == [2.4, 3, 5, 6, 5]
