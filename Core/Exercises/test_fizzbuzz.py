import pytest
from fizzbuzz import *


def test_fizzbuzz_demo():
    assert fizzbuzz(11, 20) == "11\nFizz\n13\n14\nFizzBuzz\n16\n17\nFizz\n19\nBuzz"


def test_fizzbuzz1():
    assert fizzbuzz(1, 1) == "1"


def test_fizzbuzz2():
    assert fizzbuzz(2, 2) == "2"


def test_fizzbuzz3():
    assert fizzbuzz(3, 3) == "Fizz"


def test_fizzbuzz4():
    assert fizzbuzz(4, 4) == "4"


def test_fizzbuzz5():
    assert fizzbuzz(5, 5) == "Buzz"


def test_fizzbuzz0():
    assert fizzbuzz(5, 4) == ""


def test_fizzbuzz6():
    assert fizzbuzz(6, 10) == "Fizz\n7\n8\nFizz\nBuzz"


def test_fizzbuzz7():
    assert fizzbuzz(11, 15) == "11\nFizz\n13\n14\nFizzBuzz"


def test_fizzbuzz8():
    assert fizzbuzz(15, 20) == "FizzBuzz\n16\n17\nFizz\n19\nBuzz"


def test_fizzbuzz9():
    assert fizzbuzz(1000,
                    1020) == "Buzz\n1001\nFizz\n1003\n1004\nFizzBuzz\n1006\n1007\nFizz\n1009\nBuzz\nFizz\n1012\n1013\nFizz\nBuzz\n1016\nFizz\n1018\n1019\nFizzBuzz"
