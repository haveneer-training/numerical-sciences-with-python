import pytest
from graph import *


def test_build():
    g = Tree()
    a = g.add_node("A")  # root node
    ab1 = a.add_node("AB1", 10)
    ab1c1 = ab1.add_node("AB1C1", 10)
    ab2 = a.add_node("AB2", 10)
    ab2c1 = ab2.add_node("AB2C1", 10)


def test_parent():
    g = Tree()
    a = g.add_node("A")
    assert a.parent is None
    ab1 = a.add_node("AB1", 10)
    assert ab1.parent == a
    ab1c1 = ab1.add_node("AB1C1", 10)
    assert ab1c1.parent == ab1
    ab2 = a.add_node("AB2", 10)
    assert ab2.parent == a
    ab2c1 = ab2.add_node("AB2C1", 10)
    assert ab2c1.parent == ab2


def test_build_and_print():
    g = Tree()
    a = g.add_node("A")  # root node
    ab1 = a.add_node("AB1", 10)
    ab1c1 = ab1.add_node("AB1C1", 10)
    ab2 = a.add_node("AB2", 10)
    ab2c1 = ab2.add_node("AB2C1", 10)
    assert str(g) == "(A->AB1), (AB1->AB1C1), (A->AB2), (AB2->AB2C1)"


def test_build_bad1():  # two root nodes
    g = Tree()
    g.add_node("A")
    with pytest.raises(DuplicateNodeError):
        g.add_node("B")


def test_build_bad2():  # duplicated children label in same node
    g = Tree()
    a = g.add_node("A")
    a.add_node("AB1", 10)
    with pytest.raises(DuplicateNodeError):
        a.add_node("AB1", 10)


def test_build_bad3():  # duplicated children label in different node
    g = Tree()
    a = g.add_node("A")
    ab1 = a.add_node("AB1", 10)
    ab2 = a.add_node("AB2", 10)
    ab1.add_node("C", 10)
    with pytest.raises(DuplicateNodeError):
        ab2.add_node("C", 10)


def test_print():
    g = Tree()
    a = g.add_node("A")
    ab1 = a.add_node("AB1", 10)
    ab2 = a.add_node("AB2", 10)
    ab2c1 = ab2.add_node("AB2C1", 10)


def test_access():
    g = Tree()
    a = g.add_node("A")
    ab1 = a.add_node("AB1", 10)
    ab2 = a.add_node("AB2", 10)
    ab2.add_node("AB2C1", 10)

    assert g["A"].label == "A"
    assert g["A"].parent is None
    assert g["AB1"].label == "AB1"
    assert g["AB1"].parent == g["A"]
    assert g["AB2"].label == "AB2"
    assert g["AB2"].parent == g["A"]
    assert g["AB2C1"].label == "AB2C1"
    assert g["AB2C1"].parent == g["AB2"]


def test_path():
    g = Tree()
    a = g.add_node("A")
    ab1 = a.add_node("AB1", 1)
    ab2 = a.add_node("AB2", 2)
    ab2.add_node("AB2C1", 4)

    assert g.path_length("A", "AB2C1") == 6
    assert g.path_length("AB1", "AB1") == 0
    assert g.path_length("AB1", "AB2") is None

    with pytest.raises(MissingNodeError):
        g.path_length("A", "B")
