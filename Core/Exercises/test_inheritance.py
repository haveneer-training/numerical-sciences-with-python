import pytest

from inheritance import *


def test_init_animal():
    a = Animal("a")
    assert a.name == "a"


def test_animal_bad_usage():
    with pytest.raises(AttributeError):
        a = Animal("a")
        a.name = "b"


def test_animal_is_not_a_dog():
    with pytest.raises(AttributeError):
        a = Animal("a")
        a.wafwaf()


def test_animal_is_not_a_cat():
    with pytest.raises(AttributeError):
        a = Animal("a")
        a.miaou()


def test_dog_is_an_animal():
    a = Dog("a")
    assert isinstance(a, Animal)
    a.wafwaf()


def test_cat_is_an_animal():
    a = Cat("a")
    assert isinstance(a, Animal)
    a.miaou()

