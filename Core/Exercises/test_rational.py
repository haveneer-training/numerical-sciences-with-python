import pytest
from rational import *


def test_build():
    r1 = Rational(1, 2)
    assert r1.p == 1 and r1.q == 2

    r2 = Rational(2, 4)
    assert r2.p == 1 and r2.q == 2

    r3 = Rational(2)
    assert r3.p == 2 and r3.q == 1


def test_error_build():
    with pytest.raises(InvalidRationalOperation):
        r = Rational(1, 0)
    with pytest.raises(InvalidNumberType):
        r = Rational(1.2, 1)
    with pytest.raises(InvalidNumberType):
        r = Rational(12, 1.0)


def test_add():
    r1 = Rational(1, 2)
    r2 = Rational(2, 3)

    ra1 = r1 + r1
    assert ra1.p == 1 and ra1.q == 1

    ra2 = r1 + r2
    assert ra2.p == 7 and ra2.q == 6

    ra3 = Rational(3, 12) + Rational(5, 6)
    assert ra3.p == 13 and ra3.q == 12


def test_mixed_add():
    r1 = Rational(1, 2)

    ra1 = r1 + 1  # __add__ Rational + int
    assert ra1.p == 3 and ra1.q == 2

    ra2 = 1 + r1  # __radd__ int + Rational
    assert ra2.p == 3 and ra2.q == 2


def test_sub():
    r1 = Rational(1, 2)
    r2 = Rational(2, 3)

    ra1 = r1 - r1
    assert ra1.p == 0 and ra1.q == 1

    ra2 = r1 - r2
    assert ra2.p == -1 and ra2.q == 6

    ra3 = Rational(3, 12) - Rational(5, 6)
    assert ra3.p == -7 and ra3.q == 12


def test_mult():
    r1 = Rational(1, 2)
    r2 = Rational(2, 3)

    ra1 = r1 * r1
    assert ra1.p == 1 and ra1.q == 4

    ra2 = r1 * r2
    assert ra2.p == 1 and ra2.q == 3

    ra3 = Rational(3, 12) * Rational(5, 6)
    assert ra3.p == 5 and ra3.q == 24


def test_div():
    r1 = Rational(1, 2)
    r2 = Rational(2, 3)

    ra1 = r1 / r1
    assert ra1.p == 1 and ra1.q == 1

    ra2 = r1 / r2
    assert ra2.p == 3 and ra2.q == 4

    ra3 = Rational(3, 12) / Rational(5, 6)
    assert ra3.p == 3 and ra3.q == 10


def test_str():
    r1 = Rational(1, 2)
    assert str(r1) == "1/2"

    r2 = Rational(-4, 6)
    assert str(r2) == "-2/3"

    r3 = Rational(6)
    assert str(r3) == "6"

    r4 = Rational(6, -1)
    assert str(r4) == "-6"
