import tempfile
from summarize import *


def test_summarize():
    f = tempfile.NamedTemporaryFile()
    content = """
    # e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
     #: this is my signature
    #comment UTF-8 ≤A≥
    
    #comment UTF-8 ≤A≥
    """
    f.write(bytes(content, 'utf-8'))
    # print(f.name)
    files = (f.name, f.name, f.name)
    timestamp = "2020-11-25 11:00:49.180693"
    assert summarize(files, timestamp)[0] == "59c50ffb28a3f515a1b756f833b7596a3c16bcc4af7e67a4c72cbbbe80f84512"
    assert verify_signature(content)


if __name__ == '__main__':
    test_summarize()
