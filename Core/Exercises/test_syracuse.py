from syracuse import *
import numpy as np


def test_syracuse1():
    assert np.array_equal(list(syracuse(10)), [10, 5, 16, 8, 4, 2, 1])


def test_syracuse0():
    assert len(list(syracuse(-1))) == 0


def test_syracuse2():
    import itertools
    generator = syracuse(10000000)  # lazy computation: do not compute eveythind
    top5 = itertools.islice(generator, 5)  # grab the first five elements
    assert np.array_equal(list(top5), [10000000, 5000000, 2500000, 1250000, 625000])


def test_syracuse3():
    assert len(list(syracuse(8400511))) == 686
