import math


class Vector:
    def __init__(self, values=None, size=None):
        if values is None and size is None:
            raise ValueError("Requires values or size")

        if values is not None:
            Vector.__check_values(values)
            self.values = values.copy()
            self.__size = len(self.values)
        elif size is not None:
            if not isinstance(size, int):
                raise TypeError("Vector initialization requires a list of numbers or a size")
            if size < 0:
                raise ValueError("Size must be positive")
            self.__size = size
            self.values = [0] * size

    @staticmethod
    def __check_values(values):
        if not isinstance(values, list):
            raise TypeError("Vector initialization requires a list of numbers or a size")
        for i, x in enumerate(values):
            if not isinstance(x, (int, float)):
                raise TypeError("Bad data type")

    def __radd__(self, other):
        if not isinstance(other, (int, float)):
            raise TypeError
        return Vector([x + other for x in self.values])

    def __mul__(self, other):
        if not isinstance(other, (int, float)):
            raise TypeError
        return Vector([x * other for x in self.values])

    def norm2(self):
        return math.sqrt(dot(self, self))

    def __truediv__(self, other):
        if not isinstance(other, (int, float)):
            raise TypeError
        return Vector([x / other for x in self.values])

    def __repr__(self):
        return f"Vector([{', '.join(str(x) for x in self.values)}])"

    def __str__(self):
        return f"Vector([{', '.join(str(x) for x in self.values)}])"

    def __getitem__(self, item):
        # On délègue à la liste la validation de l'index
        return self.values[item]

    def __setitem__(self, item, value):
        # On vérifie aussi le type de données
        if not isinstance(value, (int, float)):
            raise TypeError("Bad data type")
        self.values[item] = value

    def __get_size(self):
        return self.__size

    size = property(__get_size, None)


def dot(self, other):
    if not isinstance(self, Vector) or not isinstance(other, Vector):
        raise TypeError
    if not self.size == other.size:
        raise ValueError
    return sum((xi * yi for xi, yi in zip(self.values, other.values)))


# Ce main n'est appelé que si le module est exécuté directement
if __name__ == '__main__':
    # les tests et leurs vérifications
    v1 = Vector([1, 2, 3, 4])  # v1.values == [1,2,3,4]; v1.size == 4
    assert v1.values == [1, 2, 3, 4] and v1.size == 4
    v2 = Vector(size=4)  # v2.values == [0,0,0,0]; v2.size == 4
    assert v2.values == [0, 0, 0, 0] and v2.size == 4
    v3 = 2 + v2  # v3.values == [2,2,2,2]; v3.size == 4
    assert v3.values == [2, 2, 2, 2] and v3.size == 4
    v4 = v3 * 2  # v4.values == [4,4,4,4]; v4.size == 4
    assert v4.values == [4, 4, 4, 4] and v4.size == 4
    r1 = dot(v1, v4)  # produit scalaire: r1 == 40
    assert r1 == 40
    r2 = v4.norm2()  # norme euclidienne: r2 == 8
    assert r2 == 8
    v1 /= r2  # division sur place par un scalaire
    assert v1.values == [0.125, 0.25, 0.375, 0.5]
    print(v1)  # affiche: Vector([0.125,0.25,0.375,0.5])
    assert str(v1) == "Vector([0.125, 0.25, 0.375, 0.5])"

    # Test list argument
    # Pour les cas anormaux / d'exception
    # Une autre approche pourrait être de ne rien vérifier du tout dans le code de Vector
    # et d'attendre les exceptions naturelles de Python quand les opérations deviendront invalides
    try:
        Vector("want a list")
    except TypeError as err:
        print("[OK]: list expected")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on expected list")
    else:
        print("[FAILED]: missing exception on expected list")

    # Test list of numbers argument
    try:
        Vector(values=[1, 2, 3.14, "bad"])
    except TypeError as err:
        print("[OK]: list of numbers expected")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on expected list of numbers")
    else:
        print("[FAILED]: missing exception on expected list of numbers")

    # Test init by size
    try:
        Vector(size="bad")
    except TypeError as err:
        print("[OK]: int size expected")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on expected int size")
    else:
        print("[FAILED]: missing exception on expected int size")

    try:
        Vector(size=-2)
    except ValueError as err:
        print("[OK]: positive int size expected")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on expected positive int size")
    else:
        print("[FAILED]: missing exception on expected positive int size")

    # Test scalar addition
    try:
        "bad" + Vector(size=2)
    except TypeError as err:
        print("[OK]: raddition argument")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on raddition argument")
    else:
        print("[FAILED]: missing exception on raddition argument")

    # Test scalar multiply
    try:
        Vector(size=2) * "bad"
    except TypeError as err:
        print("[OK]: multiply argument")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on multiply argument")
    else:
        print("[FAILED]: missing exception on multiply argument")

    # Test division
    try:
        Vector(size=2) / "bad"
    except TypeError as err:
        print("[OK]: division argument")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on division argument")
    else:
        print("[FAILED]: missing exception on division argument")

    # Dot function
    try:
        dot(Vector(size=2), "bad")
    except TypeError as err:
        print("[OK]: dot argument")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on dot argument")
    else:
        print("[FAILED]: missing exception on dot argument")

    # Bad sizes
    try:
        dot(Vector(size=2), Vector(size=3))
    except ValueError as err:
        print("[OK]: compatible size check")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on compatible size check")
    else:
        print("[FAILED]: missing exception on compatible size check")

    # Bad index
    try:
        v1[4]
    except IndexError as err:
        print("[OK]: index validity check")
    except:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on index validity check")
    else:
        print("[FAILED]: missing exception on index validity check")

    # Bad value update
    try:
        v1[3] = (1,)
    except TypeError as err:
        print("[OK]: value type check")
    except Exception as e:  # fallback exception
        print(f"[FAILED]: bad exception '{e}' on value type check")
    else:
        print("[FAILED]: missing exception on value type check")
