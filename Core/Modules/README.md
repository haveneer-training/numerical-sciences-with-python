```
├── microlib_flat         # a package defined by a __init__.py and module files 
├── microlib_nested       # package for nested modules
│   ├── outerlib_loader   # it will load module out of current module tree
│   └── submodules        # sub-package with with modules
├── microlib_trivial      # a package defined by a directory and a module
├── module_example
├── outerlib              # package loaded by microlib_nested.outerlib_loader to show lib loading from any location
├── trivial               # a trivial module with its main program (shows conflicts between module declarations)
└── microlib_usage.py     # main program for all microlib_* example
```

Even if it is not mandatory, using a `__init__.py` is a good practice.

It could also help to restrict loaded symbols visibility using `__all__`

Tips:
```python
import module_name

# To force module reload
from importlib import reload
module_name=reload(module_name)
```