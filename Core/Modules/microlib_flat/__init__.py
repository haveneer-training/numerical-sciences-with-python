# This is a package

def describe():
    return "flat_function"


import pathlib

filename = pathlib.Path(__file__).name
if __name__ == '__main__':
    print(f"Direct lib call of {filename}")
else:
    print(f"Indirect lib call of {filename}")
