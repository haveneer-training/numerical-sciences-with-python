import pathlib
directory_installation = pathlib.Path(__file__).parent

print(f"Loading __init__.py in {directory_installation.name}")

def describe():
    return "nested_function"

# et on l'ajoute au chemin de recherche des modules
# pour charger implicitement les modules spam et eggs
# en chargeant juste microlib_nested
import sys
directory_installation = directory_installation / 'submodules'
sys.path.append(str(directory_installation))
print(sys.path)

import spam
import eggs