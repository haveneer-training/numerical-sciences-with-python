from pathlib import Path

directory_installation = Path(__file__).parent

print(f"Loading __init__.py in {directory_installation.name}")

# on récupère le répertoire où est installé le point d'entrée
outerlib_path = Path(__file__).parent.parent / 'outerlib'

# et on l'ajoute au chemin de recherche des modules
import sys

sys.path.append(str(outerlib_path))

from outerlib import *  # * but with a __all__ filter
