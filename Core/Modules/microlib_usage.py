# This is the main program (a module like others with a *main*)

def main_trivial():
    print("Trivial package without __init__.py")
    import microlib_trivial
    import microlib_trivial.eggs
    print("\t", microlib_trivial.eggs.recipes())
    print()


def main_flat():
    print("Simple package with __init__.py")
    import microlib_flat
    print(microlib_flat.describe())
    import microlib_flat.spam
    import microlib_flat.eggs
    print("\t", microlib_flat.eggs.recipes())
    print()


def main_nested_package():
    import microlib_nested
    print(microlib_nested.describe())
    # nested imports have been done in __init__.py
    print("\t", microlib_nested.eggs.recipies())
    print()


def main_nested_module():
    import microlib_nested.submodules
    print(microlib_nested.describe())
    # nested imports have been done in __init__.py
    print("\t", microlib_nested.eggs.recipies())
    print()


def main_nested_module_outerlib():
    import microlib_nested.outerlib_loader
    # nested imports have been done in __init__.py
    try:
        microlib_nested.outerlib_loader.public_variable
        msg = "is accessible"
    except AttributeError:
        msg = "is NOT accessible"
    except:
        msg = " throw an unexpected exception"
    finally:
        print(f"\tmicrolib_nested.outerlib_loader.public_variable is {msg}")

    try:
        microlib_nested.outerlib_loader.private_variable
        msg = "is accessible"
    except AttributeError:
        msg = "is NOT accessible"
    except:
        msg = " throw an unexpected exception"
    finally:
        print(f"\tmicrolib_nested.outerlib_loader.private_variable is {msg}")




import pathlib

filename = pathlib.Path(__file__).name

# Can explain if this module has been loaded as a main program or as a module
if __name__ == '__main__':
    print(f"Direct usage call of {filename}")
    main_trivial()
    main_flat()
    main_nested_package()
    main_nested_module()  # load __init__.py in cascade except for already loaded module
    #                     # (comment out previous line to see it)
    main_nested_module_outerlib()
else:
    print(f"Lib usage call {filename}")
