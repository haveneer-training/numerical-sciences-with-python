print("Loading outerlib")

__all__ = ['public_function', 'public_variable']

private_variable = "PRIVATE"

public_variable = "PUBLIC"

import sys
# print("PYTHONPATH=", os.environ.get('PYTHONPATH'))
# print("sys.path=\n ", "\n  ".join(sys.path))


def public_function():
    pass
