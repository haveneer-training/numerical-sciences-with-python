# This is the main program (a module like others with a *main*)

local_var = __file__

import mymodule  # import module with mymodule qualifier

print(mymodule.f("import mymodule"))
if local_var == __file__:
    print(f"Local var is safe\n")
else:
    print(f"Local var is NOT SAFE\n")

from mymodule import f  # import only one entity without mymodule qualifier

print(f("from mymodule import f"))
if local_var == __file__:
    print(f"Local var is safe\n")
else:
    print(f"Local var is NOT SAFE\n")

from mymodule import *  # import everything without mymodule qualifier
#                       # local definitions have been overwritten by module definitions
#                       # BAD USAGE

print(f("from mymodule import *"))
if local_var == __file__:
    print(f"Local var is safe\n")
else:
    print(f"Local var is NOT SAFE\n")
