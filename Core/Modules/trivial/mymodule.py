# this is a module

local_var = __file__


def f(msg):
    import pathlib
    filename = pathlib.Path(__file__).name
    return f"{filename}: Hello from '{msg}'"
