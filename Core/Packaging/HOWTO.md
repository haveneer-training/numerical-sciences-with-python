## Configure your package using `setup.py` file

More info: https://docs.python.org/3/distutils/setupscript.html

## Define external requirements

### `requirements-dev.txt` explains packages requirements for the package build process.

You have to install it to be able to build the package

```shell
pip3 install -r dev-requirements.txt
```

### `requirements.txt` explains package requirements to install to build package.

It is not required to install them for the packaging process. It will be done by the packaging process itself.     

```shell
pip3 install -r requirements.txt
```

## Add additional (non-python) file using `MANIFEST.in` file

`MANIFEST.in` explains file to include or exclude in the built package. It could be useful to embed non-python file into the package.

More info:  https://packaging.python.org/en/latest/guides/using-manifest-in/

## Build the package

It will produce a `.whl` file into `dist` directory.

If `install_requires` has been defined in `setup.py`, the related packages will be installed when required when installing the package.

### New fashion
(more robust using a venv isolated environment)

```shell
python3 -m build
```

### Old fashion

```shell
python3 setup.py bdist_wheel
```

## Installing the package

```shell
pip3 install package.whl
```
Example:
```shell
pip3 install package-0.1.0-py3-none-any.whl                                                                                                 80% (8:20)    2639  17:59 
```

## Moving from old fashion

| setup.py command                       | New command                                                        |
|----------------------------------------|--------------------------------------------------------------------|
| setup.py sdist<br>setup.py bdist_wheel | python -m build (with build)                                       |
| setup.py test                          | pytest                                                             |
| setup.py install                       | pip install                                                        |
| setup.py develop                       | pip install -e                                                     |
| setup.py upload                        | twine upload (with twine)                                          |
| setup.py check                         | twine check (this doesn't do all the same checks but it's a start) |
