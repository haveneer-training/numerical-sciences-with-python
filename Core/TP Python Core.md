﻿# Les fondamentaux de Python

(et déjà un peu plus)

## Consignes générales

- Tout texte écrit avec une `police à pas fixe` doit être repris littéralement soit dans votre code soit en tant que
  commande. Tout texte écrit avec une *police en italique* demande une adaptation de votre part (adaptation qui peut
  être à interpréter en fonction du contexte).
- La version minimum de Python requise est la 3.7. Vous pouvez vérifier votre version grâce à la commande `python -V`
- Nous appliquerons les règles de développement de Python décrit
  dans [PEP8 standards](https://www.python.org/dev/peps/pep-0008/). Pour vous y aider, configurez votre éditeur en
  conséquence. Vous pouvez aussi installer [pycodestyle](https://pypi.org/project/pycodestyle) qui permet de vérifier la
  conformité de votre code avec ces règles.
- La structure même du cours ne peut couvrir toutes les situations que vous rencontrerez dans votre parcours. Le cours
  est avant un tremplin vous exposant les lignes directrices et les particularités moins évidentes. Python est un
  langage populaire et il existe beaucoup de ressources disponibles sur internet. N’hésitez jamais à y avoir recours (
  c’est d’ailleurs une pratique requérant aussi de l'entraînement au niveau du vocabulaire, de la pratique récurrente de
  l’anglais et l’analyse critique des *réponses* que vous y trouverez). Ne recopiez jamais du code sans comprendre ce
  que vous faîtes. Certaines commandes ou certains scripts pourraient tout simplement endommager votre système.

## Consignes particulières

- Vous n’avez ici le droit à aucune autre bibliothèque que celles incluses par défaut dans Python (en particulier, vous
  n’avez pas le droit à `numpy` ou à `pandas`). Des restrictions particulières pourront s’appliquer au cas par cas.
- Les signatures de fonctions, classes ou méthodes doivent être scrupuleusement respectées.

## Exercice 1 : FizzBuzz Game

Fichier `fizzbuzz.py`

Écrivez une fonction `fizzbuzz(n1,n2)` qui retourne une chaîne de caractères (de *n2-n1+1* lignes) où pour chaque ligne
nous avons une des valeurs suivantes:

- `Fizz` si *x* est divisible par 3
- `Buzz` si *x* est divisible par 5
- `FizzBuzz` si *x* est divisible par 3 et par 5
- *x* sinon

avec *x* correspondant à l’indice de la ligne, parcourant les valeurs de *n1* à *n2* (inclus).

Exemple: `fizzbuzz(11, 20)` retourne la chaîne de caractères:

```
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
```

(chaque retour à la ligne est un retour de type `"\n"`).

# Exercice 2 : Le vol de Syracuse (2 points)

Fichier `syracuse.py`

Créez une fonction génératrice `syracuse(n)` qui en partant d’un entier strictement positif *n* calcule à chaque
nouvelle itération l’élément (toujours un entier) suivant de la suite de Syracuse.

La suite de Syracuse est défini comme suit:

- on part d'un nombre entier strictement positif
- s’il est pair, on le divise par 2
- s’il est impair, on le multiplie par 3 et on ajoute 1

Notre fonction génératrice `syracuse(n)` doit s’arrêter après avoir atteint la valeur 1 et s’arrête immédiatement si *n*
est plus petit que 1. Si *n* est strictement positif, le premier appel retourne l’entier *n* lui-même.

Exemple:

| **Code**                                                   | **Affichage / Résultat**          |
|:-----------------------------------------------------------|:----------------------------------|
| len(list(syracuse(0))                                      | 0                                 |
| for x in syracuse(10):<br>&nbsp;&nbsp;&nbsp;&nbsp;print(x) | 10<br>5<br>16<br>8<br>4<br>2<br>1 |
| len(list(syracuse(10))                                     | 7                                 |

## Exercice 3 : extraction de valeurs

Fichier `extract_values_in_range.py`

Définissez une fonction `extract_values_in_range` qui prend en arguments:

- une liste Python `data` (ce n’est pas un tableau numpy)
- une valeur `minv`
- une valeur `maxv`

Cette fonction retourne une nouvelle liste contenant tous les éléments *x* de data tels que *minv < x < maxv*.

Si une erreur se produit pour une quelconque raison, le résultat retourné doit être `None`.

## Exercice 4 : calcul sur des rationnels

Fichier `rational.py`

Définissez une classe `Rational` qui décrit un nombre rationnel.

Par convention, tout nombre rationnel doit toujours être représenté soit sous la forme d’un entier soit sous la forme
d’un quotient irréductible p/q tel qu’il n’existe aucun diviseur (positif) commun entre p et q (à l’exception de 1).
Toujours par convention, nous imposerons que q est strictement positif.

Les objets de type `Rational` doivent supporter les opérations suivantes:

- création d’un `Rational` à partir de deux entiers p et q
  (`p` et `q` doivent être le nom des deux attributs)
  Attention: p/q n’est peut-être pas encore une forme irréductible
- création d’un `Rational` à partir d’un unique entier p
- somme de deux `Rational`s ou bien d’un `Rational` et d’un entier
- différence de deux `Rational`s ou bien d’un `Rational` et d’un entier
- produit de deux `Rational`s ou bien d’un `Rational` et d’un entier
- quotient de deux `Rational`s ou bien d’un `Rational` et d’un entier
- conversion en chaîne de caractères de la forme `p/q` si le dénominateur est différent de 1 et `p` s’il vaut 1.

Si à la place d’un entier, c’est un nombre flottant qui est donné, une exception de type `InvalidNumberType` doit être
générée.

Si une opération est invalide pour une quelconque autre raison, c’est une exception de type `InvalidRationalOperation`
qui doit être générée.

## Exercice 5 : une classe *vecteur*

Fichier `vector.py`

- Créez une classe `Vector` prenant en argument nommé `values` un tableau de valeurs numériques représentant un vecteur
  de $R^n$.

Si l’une des valeurs n’est pas numérique, une exception de type `TypeError` doit être retournée en précisant l’élément
détecté comme invalide.

La classe Vector doit aussi supporter une construction à partir d’un attribut `size` qui définit la taille du vecteur à
créer, toutes les valeurs devant alors être égales à `0`.

- La classe `Vector` a deux attributs `values` et `size` qui retournent respectivement la liste des valeurs et la taille
  du vecteur.
- Ajoutez-y toutes les opérateurs vectoriels nécessaires pour permettre le code suivant:
  ```python
  v1 = Vector([1,2,3,4]) # v1.values == [1,2,3,4]; v1.size == 4
  v2 = Vector(size=4)    # v2.values == [0,0,0,0]; v2.size == 4
  v3 = 2 + v2            # v3.values == [2,2,2,2]; v3.size == 4
  v4 = v3 * 2            # v4.values == [4,4,4,4]; v4.size == 4
  r1 = dot(v1, v4)       # produit scalaire*: r1 == 40
  r2 = v4.norm2()        # norme euclidienne: r2 == 8
  v1 /= r2               # division *sur place* par un scalaire
  print(v1)              # affiche: Vector([0.125, 0.25, 0.375, 0.5])
  ```

N’hésitez à faire un tour ici : <https://docs.python.org/3/reference/datamodel.html#specialnames> (en particulier en
regardant autour de `__radd__`, `__str__` …)

- Tout calcul avec des vecteurs de dimensions incompatibles doit retourner une exception de type `ValueError`. Comment
  testez-vous ce comportement avec votre implémentation ?
- Votre classe `Vector` doit être dans un module nommé `vector.py` et s’il est exécuté directement il doit lancer vos
  tests de validation.

## Exercice 6 : une classe *matrice*

Fichier `matrix.py`

- Créez une classe `Matrix` prenant en argument nommé `values` un tableau de tableaux de valeurs numériques représentant
  une matrice de $R^n$ dans $R^m$ (n lignes, m colonnes).

  Si l’une des valeurs n’est pas numérique, une exception de type `TypeError` doit être retournée en précisant l’élément
  détecté comme invalide. Si les dimensions données pour les tableaux ne permet de former une matrice rectangulaire, une
  exception de type $ `ValueError` doit être retournée en précisant l’anomalie constatée.

  La classe `Matrix` doit aussi supporter une construction à partir d’un attribut `shape` qui est un `tuple` qui définit
  les dimensions (*n*, *m*) de la matrice, toutes les valeurs devant alors être égales à 0.

- La classe `Matrix` a deux attributs `values` et `shape` qui retournent respectivement la liste des valeurs et les
  dimensions de la matrice (sous la forme d’un tuple (*n*,*m*)).
- Ajoutez-y toutes les opérateurs vectoriels nécessaires pour permettre le code suivant:

  ```python
  m1 = Matrix([[0.0, 1.0, 2.0, 3.0], # m1.shape == (2,4)
               [0.0, 2.0, 4.0, 6.0]]) 
  m2 = Matrix([[0.0, 1.0],           # m2.shape == (4,2)
               [2.0, 3.0], 
               [4.0, 5.0],
               [6.0, 7.0]])
  m3 = m1 * m2          # m3.values == [[28,34],[56,68]]
  print(m3)             # affiche: Matrix([[28,34],[56,68]])
  v1 = (m2 * m1).diag() # retourne la diagonale du produit entre m2 et m1
                        # v1.values == [0, 8, 28, 60]; v1.size == 4
  v2 = m1 * v1          # v2.size == 2
  print(v2)             # affiche: Vector([244, 488])
  ````

N’hésitez à faire un tour ici : <https://docs.python.org/3/reference/datamodel.html#specialnames> (en particulier en
regardant autour de `__mul__`, `__str__` …)

- Tout calcul avec des dimensions incompatibles doit retourner une exception de type `ValueError`. Comment testez-vous
  ce comportement avec votre implémentation ?
- Votre classe `Matrix` doit être dans un module nommé `matrix.py` et s’il est exécuté directement il doit lancer vos
  tests de validation.
- Sachant que
  ```python
  vdata = [1, 2, 3, 4]
  mdata = [[0.0, 1.0, 2.0, 3.0],
           [0.0, 2.0, 4.0, 6.0]]
  v1 = Vector(vdata)
  m1 = Matrix(mdata)
  ```
  Que se passe-t-il dans ces deux situations ?
    - Situation 1
  ```python
  vdata[1] = 0
  mdata[1][3] = 0
  v2 = m1 * v1 
  print(v2) # affiche: Vector([...])</p>|
  ```
    - Situation 2
  ```python
  vdata.append(5)
  mdata[1].append(0)
  v2 = m1 * v1
  print(v2) # affiche: Vector([*...*])
  ```
  Est-ce le résultat *naturellement* attendu ? Si non, que pouvez-vous y faire pour y remédier ?

Bonus:

- Comment faire pour que la taille (`size` ou `shape`) du vecteur et de la matrice correspondent bien toujours à la
  taille de la liste de valeurs? Que se passe-t-il si l’attribut `values` ou size est modifié directement depuis
  l’extérieur de la classe ?
- Créez une classe d'exception `MalformedDataError` (héritant de classe standard `Exception`) permettant d’identifier
  les erreurs liées aux données *non rectangulaires*.

## Exercice 7 : une histoire d’héritage

Fichier `inheritance.py`

Créez une classe `Animal` qui se construit à partir d'un argument `name` qui représente le nom de l'animal.

Le nom (en tant qu’attribut et non méthode) doit être interrogeable en lecture mais non modifiable depuis l'extérieur de
la classe.

Créez une classe `Dog` qui hérite de la classe `Animal` et qui se construit également à partir d'un nom. Le `Dog` doit
répondre à l'attribut `name` tout comme `Animal`.

`Dog` possède également une méthode `wafwaf()` qui affiche *WafWaf* à l'écran.

Créez une classe `Cat` qui hérite de la classe `Animal` et qui se construit également à partir d'un nom. Le `Cat` doit
répondre à l'attribut `name` tout comme `Animal`.

`Cat` possède également une méthode `miaou()` qui affiche *Miaou* à l'écran.

## Exercice 8 : lecture de données CSV

Fichier `dataset.py`

- En utilisant le module `csv` (<https://docs.python.org/3/library/csv.html>), lisez un jeu de données au format CSV.
  Pour l’occasion, vous allez découvrir
  le [Context Manager](https://book.pythontips.com/en/latest/context_managers.html) avec le mot clef `with`. et quelques
  manipulations plus avancées sur les chaînes de caractères. Nous allons nous restreindre à des données numériques
  réelles homogènes par colonne (*i.e.* les séries représentées par colonnes). La suite du sujet sera illustré avec les
  données ci-dessous (`data.csv`) issues de [Spurious Correlations](https://www.tylervigen.com/spurious-correlations):
  ```csv
  columns, Margarine consumed (lbs), Divorce rate in Maine (/1000)
  2000,8.2,5
  2001,7,4.7
  2002,6.5,4.6
  2003,5.3,4.4
  2004,5.2,4.3
  2005,4,4.1
  2006,4.6,4.2
  2007,4.5,4.2
  2008,4.2,4.2
  2009,3.7,4.1
  ```
  Données originales:
    - [National Vital Statistics Reports](https://www.cdc.gov/nchs/data/dvs/state-divorce-rates-90-95-99-18.pdf)
    - [U.S. Department of Agriculture](https://www2.census.gov/library/publications/2011/compendia/statab/131ed/tables/12s0217.xls)

- Créez une classe `Dataset` qui prend en entrée un nom de fichier CSV et qui construit une représentation interne sous
  forme de `Matrix` des données et deux listes d’étiquettes (index pour les lignes et columns pour les colonnes). Vous
  utiliserez le module standard cvs (<https://docs.python.org/fr/3.8/library/csv.html>) pour lire le fichier.
  ```python
  d = Dataset("data.csv") # Retourne une exception ValueError si
                          # les contraintes ne sont pas respectées
  print(d.columns)       # affiche: ['Margarine consumed…', 'Divorce rate…']
  print(d.index)         # affiche: ['2000', '2001', '2002', '2003'...]
  print(d[d.columns[0]]) # affiche: Vector([8.2, 7.0, 6.5, 5.3, 5.2...])</p>|
  ```

- Votre classe Dataset doit être dans un module nommé dataset.py et s’il est exécuter directement il doit lancer vos
  tests de validation sur un fichier CSV fourni en argument.

- Dans un module stat\_tools.py, ajoutez les fonctions libres (hors toute classe) suivantes:
    - mean(*x*) : calcul de la moyenne du tableau *x*
    - median(*x*) : calcul de la médiane du tableau *x*
    - std(*x*) : calcul de l’écart type du tableau *x*

  Elles prennent toutes un tableau de réels en arguments et retourne le réel correspondant si le tableau n’est pas
  vide, `None` s’il est vide.

- Ajouter à la classe `Dataset`, une méthode `describe()` qui fait un résumé statistique de la forme:
  ```python
  d = Dataset("data.csv")
  print(d.describe()) # affiche (ci-dessous)
  #           Margarine consumed (lbs) Divorce rate in Maine (/1000) 
  # mean:                          5.3                           4.4 
  # median:                        4.9                           4.2 
  # std:                           1.4                          0.28
  ```

- Définissez un itérateur pour parcourir les différentes séries (colonnes). Chaque élément devra fournir un attribut
  title (chaîne de caractères) décrivant le titre de la série et un attribut values (de type Vector) décrivant les
  valeurs de la série.
  ```python
  d = Dataset("data.csv")
  for column\_iter in d:
    print(column\_iter.title)
    print(f"\t{column\_iter.values}")
  # Affiche:
  # Margarine consumed (lbs)
  # 	Vector([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
  # Divorce rate in Maine (/1000)
  # 	Vector([5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])
  ```

## Exercice 9 : Construction d’une régression linéaire

Fichier `linear_regression.py`

Afin de réduire la complexité algorithmique, nous allons ici nous limiter à une régression très simple.

En partant de deux séries de données x et y, nous allons calculer une régression linéaire de la forme
$f=\hat{\alpha}+\hat{\beta}x$ avec où $\hat{\alpha}$ et $\hat{\beta}$ sont obtenus par minimisation de la somme des
résidus (au carré).

Les calculs effectifs de ces coefficients sont à extraire des liens fournis ci-dessous.

- Régression linéaire simple: <https://en.wikipedia.org/wiki/Simple_linear_regression>
- Calcul du R2 : <https://en.wikipedia.org/wiki/Coefficient_of_determination>

Nous prendrons de nouveau les données du jeu de données `data.csv` pour cet exercice.

Dans un module `linear_regression.py`:

- Définissez une fonction `compute_regression_coefficients` qui, à partir de deux séries `x` et `y`, calcule les
  coefficients $\hat{\alpha}$ et $\hat{\beta}$ et qui sont retournés sous la forme d’un tuple.
- Calculez le coefficient de détermination $R^2$ qui mesure la *qualité* de la prédiction de la régression linéaire.
- Tracez les données et la droite de régression grâce à la bibliothèque [matlibplot](https://matplotlib.org).

  Sachant que [matlibplot](https://matplotlib.org) ne sera développé que plus tard dans le cours, l’exemple ci-dessous,
  qui couvre votre besoin actuel, vous est fourni pour vous guider:
  ```python
  import matplotlib.pyplot as plt
  
  if __name__ == '__main__':
      x = [1, 2, 3, 4, 10]
      y = [2, 4, 5.5, 8.5, 19]
      y_pred = [2, 4, 6, 8, 20]
  
      plt.scatter(x, y, color="r", marker="o")
      plt.xlabel("X label")
      plt.ylabel("y label")
      plt.title("Titile")
      plt.plot(x, y_pred, color="b")
      plt.show()
  ```

- Votre programme `linear_regression.py` doit prendre en argument un nom de fichier CSV et doit ensuite produire le
  calcul de la régression linéaire, du $R^2 et tracer le tout. Ainsi la ligne de
  commande `python3 linear_regression.py data.csv` doit écrire sur la console:
  ```
  Estimated coefficients:
  alpha = 3.308626396359138
  beta = 0.20138601572196674
  R^2 = 0.9851722929916441
  ```
  et afficher le tracé:

  ![](images/fb5123cb-89c6-499e-b600-be9424e5d9c4.001.png)

## Exercice 10 : construction d’un graphe

Fichier `graph.py`

L’objet est ici de construire un type particulier de graphe que sont les arbres.

![](Images/graph.png)

Un tel arbre est composé:

- d’un ensemble de noeuds
- d’un ensemble d’arêtes portant un poids numérique

Chaque nœud peut avoir des sous-nœuds (enfants; sans limite de nombre) en passant par des arêtes.

Chaque nœud a au plus un nœud parent; un seul nœud n’a pas de parent, c’est le nœud racine (A sur la figure).

Nous y ajoutons quelques contraintes:

- Chaque nœud a un label unique à tout l’arbre
- Il ne peut y avoir deux arêtes liant deux mêmes nœuds

1. Définissez une classe `Tree` ainsi qu’une sous-classe `Node` (sous-classe de `Tree`)
2. Ajoutez un constructeur pour `Node` qui prend (au moins; vous aurez peut-être à l’enrichir dans les questions à
   venir) en argument un `label`, et un nœud `parent`. Un `Node` expose son parent via l’attribut `parent` (qui est soit
   un autre `Node` soit `None` s’il n’y a aucun parent)
3. Ajoutez une méthode `add_node(label)` à la classe `Tree` qui définit construit le nœud racine de l’arbre. Si un nœud
   racine existe déjà une exception de type `DuplicateNodeError` doit être générée.
4. Ajoutez la méthode `add_node(label, weight)` à `Node` afin d’ajouter un sous-noeud (enfant) à un nœud. Prenez soin de
   mémoriser les enfants du nœud (et le poids porté par l’arête) pour les parcourir plus tard. Si un nœud enfant de même
   label existe déjà une exception de type `DuplicateNodeError` doit être générée.
5. Faites en sorte qu’une exception de type `DuplicateNodeError` soit générée si un nœud portant un `label` déjà présent
   dans l’arbre est créé.
6. Ajoutez une méthode de conversion de l’arbre en chaîne de caractères. Nous devons y parcourir les nœuds *en
   profondeur d’abord*, c’est-à-dire qu’avec de parcourir un autre enfant direct d’un nœud, on doit parcourir les
   sous-enfants. Par exemple, pour la figure donnée ci-dessus, cela doit donner:
   ```
   (A->B), (B->C), (B->D), (B->E), (A->F)
   ```
   Les espaces, la forme des flèches, les parenthèses doivent être reproduites à l’identique.
8. Ajoutez à `Tree` une méthode qui réponde à la signature `tree[label]` (où `tree` est une variable de type `Tree`) qui
   retrouve à partir d’un label le nœud associé dans l’arbre. S’il n’existe aucun nœud ayant le label demandé, alors une
   exception de type `MissingNodeError` doit être générée.
9. Ajoutez à `Tree` une méthode `path_length(first_label, second_label)` qui calcule le chemin entre deux nœuds
   identifiés par leurs labels. Si un chemin est trouvé, alors c’est la somme des poids (portés par les arêtes) sur le
   chemin qui est retournée et si aucun chemin n’existe, c’est la valeur `None` qui doit être retournée.

NB:
- Les exceptions `MissingNodeError` et `DuplicateNodeError` sont des types d’exception que vous devez définir
  vous-mêmes.

## Tips

Quelques éléments de documentation complémentaires:

- Pour vos tests utilisez assert (built-in) ou bien le module [pytest](https://docs.pytest.org/en/stable/) (bonus).

  Parfois la doc officielle n’est pas facile à
  lire: <https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt>

  Pensez à voir ailleurs : <https://stackoverflow.com/questions/5142418/what-is-the-use-of-assert-in-python>

- Pensez à aussi tester votre production sur des données incorrectes. Le typage dynamique vous permet d’écrire plus
  vite, mais la charge des tests n’en est que plus importante. Par exemple:
  ```python
  Vector(values=[1, 2, 3.14, "bad data"])
  ```
  devrait retourner une exception de type `TypeError`.

  Un test qui ferait cela serait:
  ```python
  try:
      Vector(values=[1, 2, 3.14, "bad data"])
  except TypeError as err:
      print("[OK]: list of numbers expected")
  except:
      print("[FAILED]: bad exception on expected list of numbers")
  else:
      print("[FAILED]: missing exception on expected list of numbers")
  ```
- Pour être dans le *style* Python, minimisez les boucles explicites et favorisez les comprehensions.
- Jeter un œil aux opérations ensemblistes [*
  built-in*](https://docs.python.org/3/library/functions.html): [all()](https://docs.python.org/3/library/functions.html#all)
  , [any()](https://docs.python.org/3/library/functions.html#any)
  , [enumerate()](https://docs.python.org/3/library/functions.html#enumerate)
  , [min()](https://docs.python.org/3/library/functions.html#min)
  , [map()](https://docs.python.org/3/library/functions.html#map)
  , [max()](https://docs.python.org/3/library/functions.html#max)
  , [sorted()](https://docs.python.org/3/library/functions.html#sorted)
  , [sum()](https://docs.python.org/3/library/functions.html#sum)
  , [zip()](https://docs.python.org/3/library/functions.html#zip) cela pourra vous éviter de réécrire des
  fonctionnalités déjà existantes en standard.
- Les options de [print()](https://docs.python.org/3/library/functions.html#print) peuvent vous fournir des moyens
  utiles pour écrire une ligne en plusieurs fois ou pour séparer différemment les arguments à écrire.
- Rejeter un coup d’oeil à property soit depuis les notes de cours soit sur le site
  officiel: <https://docs.python.org/3/howto/descriptor.html#properties>
- Pour mieux gérer les affichages (en particulier des nombres flottants), jeter un oeil au formattage des *
  f-strings* : <https://docs.python.org/fr/3/tutorial/inputoutput.html#formatted-string-literals>
- La sous-section de matplotlib que nous utilisons
  ici: <https://matplotlib.org/api/_as_gen/matplotlib.pyplot.html?highlight=matplotlib.pyplot#module-matplotlib.pyplot>
- Utiliser des arguments issus de la ligne de
  commande: <https://www.tutorialspoint.com/python/python_command_line_arguments.htm>


