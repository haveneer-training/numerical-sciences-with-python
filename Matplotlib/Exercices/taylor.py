import math
import numpy as np
import matplotlib.pyplot as plt


def P(a, N):
    derivs = [lambda x: math.sin(x),
              lambda x: math.cos(x),
              lambda x: -math.sin(x),
              lambda x: -math.cos(x)]

    def PaN_internal(x):
        r = 0
        for n in range(N + 1):
            r += derivs[n % 4](a) * np.power(x - a, n) / math.factorial(n)
        return r

    return PaN_internal


def plotP(a, h, NList):
    X = np.linspace(a - h, a + h, 100)
    plt.clf()  # clear figure
    plots = []
    for n in NList:
        Y = P(a, n)(X)
        plots.extend(plt.plot(X, Y))
    plt.legend(plots, NList)


if __name__ == '__main__':
    plotP(1, 1, [0, 1, 2, 3, 4])
