import pytest
from taylor import *
import math


def test_deriv_at0():
    assert 0 == pytest.approx(P(0, 0)(1))
    assert 0 == pytest.approx(P(0, 0)(0))
    assert 1 == pytest.approx(P(0, 1)(1))
    assert 5 / 6 == pytest.approx(P(0, 3)(1))


def test_deriv_at1():
    assert math.sin(1) == pytest.approx(P(1, 0)(1))
    assert math.sin(1) == pytest.approx(P(1, 0)(0))
    assert math.sin(1) == pytest.approx(P(1, 1)(1))
    assert math.sin(1) - math.cos(1) == pytest.approx(P(1, 1)(0))
    assert math.sin(1) / 2 - 5 * math.cos(1) / 6 == pytest.approx(P(1, 3)(0))


def test_deriv_atmulti():
    assert np.array([math.sin(1), math.sin(1)]) == pytest.approx(P(1, 0)(np.array([1, 0])))
    assert np.array([math.sin(1), math.sin(1) - math.cos(1)]) == pytest.approx(P(1, 1)(np.array([1, 0])))
    assert np.array([math.sin(1) / 2 - 5 * math.cos(1) / 6, math.sin(1)]) == pytest.approx(P(1, 3)(np.array([0, 1])))
