## Exercice 1 : approximation par série de Taylor[^5]

L’approximation par série de Taylor d’une fonction « suffisamment régulière » autour du point *a* s’écrit 

![](Images/16ff838c-8e15-4819-b003-6588a00ddf9d.004.png)

Pour des raisons de simplifications (sans réelle perte de généralités), nous nous limiterons au cas a=0. 

Notons P(x; N), le polynôme de degré N issu de la série.

Prenez une fonction dont le rayon de convergence en 0 de la série de Taylor associé est infini et une autre fonction où le rayon est fini et pour chacun de ces deux fonctions:
   1. Définissez une fonction Python `P` représentant le polynôme P(x;N) associé (c’est bien une fonction qui retourne une fonction).
      
      Un premier usage sera pour x de type flottant mais pensez aussi au cas où `x` est un tableau 1D de numpy.
   
   2. Tracez la fonction ainsi que P(x; N) pour différentes valeurs de N.
   3. Qu’observez-vous quand x s’éloigne de point de développement qui est ici a=0 ?
   4. BONUS: utilisez un *slider* pour choisir le degré N du polynôme P.

La présence d’une légende sur la figure indiquant le paramètre N des différentes courbes.

Voici deux exemples de résultats attendus:

![](Images/e9170f93-2108-4c29-9d93-fd6fec4fd64b.003.png)![](Images/e9170f93-2108-4c29-9d93-fd6fec4fd64b.004.png)   

| plotP(1, 5, range(5)) |  plotP(1, 0.5, range(5))  |
|:---------------------:|:-------------------------:|


## Exercice 2: diagramme de bifurcation de la suite logistique

Fichier `logistic_map.ipynb`

Soit la suite logistique[^12] définie par:

![](Images/16ff838c-8e15-4819-b003-6588a00ddf9d.005.png)

Suivant la valeur du paramètre μ (dans [0; 4] pour assurer que x reste dans [0; 1]), cette suite génère une suite convergente, une suite soumise à oscillations ou une suite chaotique.

Tracez le diagramme de bifurcation[^13] de cette suite.

` `![](Images/16ff838c-8e15-4819-b003-6588a00ddf9d.006.png)

[^5]: [https://fr.wikipedia.org/wiki/Série_de_Taylor](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Taylor) 
[^12]: <https://fr.wikipedia.org/wiki/Suite_logistique> 
[^13]: <https://en.wikipedia.org/wiki/Bifurcation_diagram> 

[^6]: <https://fr.wikipedia.org/wiki/Lemniscate_de_Bernoulli> 
[^7]: [https://fr.wikipedia.org/wiki/Astroïde](https://fr.wikipedia.org/wiki/Astro%C3%AFde) 
[^8]: [https://fr.wikipedia.org/wiki/Cardioïde](https://fr.wikipedia.org/wiki/Cardio%C3%AFde) 
[^9]: [https://fr.wikipedia.org/wiki/Néphroïde](https://fr.wikipedia.org/wiki/N%C3%A9phro%C3%AFde) 
[^10]: [https://fr.wikipedia.org/wiki/Clothoïde](https://fr.wikipedia.org/wiki/Clotho%C3%AFde) 
[^11]: [https://fr.wikipedia.org/wiki/Rosace_(mathématiques)](https://fr.wikipedia.org/wiki/Rosace_\(math%C3%A9matiques\)) 
