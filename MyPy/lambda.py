from typing import Callable


# https://docs.python.org/3/library/typing.html#typing.Callable
def image(f: Callable[[int], int]) -> None:
    for x in range(10):
        print(f"{x} -> {f(x)}")


if __name__ == '__main__':
    image(lambda x: x ** 2)
