from typing import List


def f(args: List[int]):
    for e in args:
        print(e, ' is a ', type(e))
    pass


def image(f: object) -> object:
    """

    :type f: function
    """
    for x in range(10):
        print(f"{f(x)}: {x}")


if __name__ == '__main__':
    s = 3
    f([1, 2, s])
    image(lambda x: x**2)
