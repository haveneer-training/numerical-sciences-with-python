property
lambda


https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/eb326b60bec3461ba2621fd4d6bd95b8/
https://github.com/jdwittenauer/ipython-notebooks # very good

# Manipulation de nombres entiers et flottants (dans REPL ou Notebook)
* Combinaison d'entiers et de flottants
* Nombres min et max, précision

# Les chaînes de caractères

* l'encodage
* les strings
* input

# Itérateurs
* En utilisant __iter__ et __next__ définissez un itérateur qui liste toutes les permutations d'un tableau 

https://www.johnwittenauer.net/machine-learning-exercises-in-python-part-1/


# See later
* sujet de C++ du mastère HPC-AI
* https://github.com/guipsamora/pandas_exercises
* Pandas: https://www.machinelearningplus.com/python/101-pandas-exercises-python/ (+++)
* https://github.com/TheAlgorithms/Python
* https://www.cs.toronto.edu/~frossard/post/linear_regression/
* http://www.python-simple.com/python-scipy/fitting-regression.php
* https://data36.com/linear-regression-in-python-numpy-polyfit/
* https://devarea.com/linear-regression-with-numpy/
* https://medium.com/analytics-vidhya/simple-linear-regression-with-example-using-numpy-e7b984f0d15e
* https://fr.wikipedia.org/wiki/Loi_de_Benford
* Update bayesiennes vs p-values (need ref /!\ )
* Jeu de la vie


Exercices tout prêt mais très unitaires
* https://www.w3resource.com/python-exercises/numpy/index.php
* https://www.w3resource.com/python-exercises/nltk/index.php
* https://www.w3resource.com/python-exercises/pandas/index.php
* https://www.w3resource.com/python-exercises/geopy/index.php
* https://www.w3resource.com/python-exercises/itertools/index.php
* https://www.w3resource.com/python-exercises/BeautifulSoup/index.php



# Projets

## Image processing
Convolution sur image: sobel filter (https://youtu.be/8rrHTtUzyZA)
https://www.pluralsight.com/guides/importing-image-data-into-numpy-arrays
Fourrier on image


Fourrier:
* https://www.youtube.com/watch?v=r6sGWTCMz2k&t=1s
* https://youtu.be/ds0cmAV-Yek
