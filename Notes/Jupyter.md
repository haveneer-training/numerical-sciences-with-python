# Installing Jupyter

## Default installation
```
pip3 install jupyterlab
pip3 install notebook
pip3 install voila

jupyter --version
jupyter labextension install @jupyterlab/plotly-extension
```

```
jupyter lab [--no-browser]
jupyter-notebook --help
```

## Installing Rust addons
```
source $HOME/.cargo/env
cargo install evcxr_jupyter
evcxr_jupyter --install
```

## Installing C++ addons
```
conda install -c conda-forge xeus-cling
```

# Magics

## ipythontutor
Depuis un notebook, `ipythontutor` permet d'évaluer du code pas-à-pas
* `%load_ext ipythontutor` : chargement
* utilisation
  ```
  %%ipythontutor height=230 ratio=0.7
  a1 = list(range(3))
  a2 = list(range(10, 13))
  a3 = a1 + a2
  ```

## timeit
Avec un seul % une commande magique concerne une seule ligne,
```
%timeit L1 = list(range(1000))
L2 = list(range(1000))
```
avec deux %, cela concerne toute la cellule
```
%%timeit # doit alors être en première ligne
L1 = list(range(1000))
L2 = list(range(1000))
```


* 