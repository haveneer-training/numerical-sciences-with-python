# Pour un training professionnel :

* Compléter la construction et la diffusion de packages
* Ajouter une mention/démo de Numba
* Voir aussi le cours "Python - perfectionnement" (dont NumPy, Pandas et Matplotlib)
* Métaclasses ?
* Profilage avec cProfile (ex: https://nbviewer.jupyter.org/github/jdwittenauer/ipython-notebooks/blob/master/notebooks/language/IPythonMagic.ipynb)
* Use http://nbviewer.ipython.org for rendereing

# Updates
## Autres intégrations dans scikit-learn
* Intégration de TensorFlow : https://github.com/skorch-dev/skorch
* Dérivation de *features* avec [TSFresh](https://tsfresh.readthedocs.io/en/latest/text/introduction.html) : https://tsfresh.readthedocs.io/en/latest/text/sklearn_transformers.html

# Suggestion de participants
## Parler de Sage (*Maple like*)
* https://www.sagemath.org
* https://github.com/sagemath/sage

## Parler de Plotly (interactive and browser-based graphing library for Python)
* https://github.com/plotly/plotly.py
* scipy+signal : https://notebook.community/pxcandeias/py-notebooks/scipy_signal_filters_demonstration  
* Scipy+sparse : https://docs.scipy.org/doc/scipy/reference/sparse.html 

# Suggestion parce que c'est cool
* scipy+spatial : https://github.com/tylerjereddy/pycon-2017/blob/master/mastering_scipy_spatial.ipynb
