## Get source code from function
```
import pprint
import inspect
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(inspect.getsource(f))
```

## Recharger un module
```
from importlib import reload
reload(multiple_import)
```
ou bien
```
del sys.modules['multiple_import']
import multiple_import
```
