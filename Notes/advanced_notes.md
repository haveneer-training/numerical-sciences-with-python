


# Advanced

## Les fonctions 𝝺

```
f = lambda x: x**2
f(2)
```

On peut aussi utiliser des variables locales dans la lambda, et les variables seront "récupérées" au moment de l'évaluation (relativement au contexte de création) 
```
cst = 1
f = lambda x: cst + x
f(1)
cst = 2
f(1)
```

```
def make_f():
    cst = 1
    return lambda x: cst + x
f = make_f()
f(1)
cst = 2 # pas d'influence car contexte différent
f(1)
```

Il existe néanmoins des astuces pour forcer la capture:
```
cst = 1
f = lambda x, cst=cst: cst + x #  recopie de la valeur à capture en tant que variable additionnelle avec une valeur par défaut
f(1)
cst = 2
f(1)
```
ou bien
```
cst = 1
f = (lambda cst: lambda x: cst + x)(cst) 
f(1)
cst = 2
f(1)
```
Et s'il vous faut une capture en écriture:
```
# Mettre progressivement les modifs locales et le nonlocal
def action_counter(title):
    counter = 0
    cumul = 0
    def incr(val):
        nonlocal counter, cumul
        counter += 1
        cumul += val
        print(f"{title}: current cumul: {cumul} after {counter} calls")
    return incr
f=action_counter("Title")
f(1)
f(10)
```
NB: L'attribut __closure__ d'une fonction donne accès aux éléments de la closure.

## Un style plus fonctionnel en built-ins:
cf https://docs.python.org/release/3.1.3/library/functions.html
* all
* any
* min, max, sum
* zip
* enumerate
* map
* filter
* join
* sorted
* reversed
* module itertools : https://docs.python.org/release/3.1.3/library/itertools.html

## Advanced : __hash__
Vous pouvez aussi définir `__eq__` pour comparer des Points.

Si vous voulez définir `__hash__` (pour que l'objet puisse entrer dans un set), faites attention aux conséquences:
```
class Thing:
    data = ""
    def __init__(self, v):
        self.data = v
    def __repr__(self):
        return f"Thing({self.data})"
    def __eq__(self, other):
        return self.data == other.data
    def __hash__(self):
        h = 0
        for ch in self.data:
            h += ord(ch) 
        return h
```

```
# /!\ hash sur des objets mutables dans des sets 
p1,p2 = Thing("abc"), Thing("abc")
s={p1,p2}
p1 in s # True 
p2 in s # True
p1.data = "new"
p1 in s # False car p1 est rangé dans le set à l'emplacement de son ancien hash 
p2 in s # False car p2 a bien un hash présent dans le set mais p2 n'est pas égal (cf __eq__) à l'élément présent
```


## Les décorateurs
```
def foo():
    # do something
    print("foo")

def decorator(func):
    # manipulate func
    def nested(*args, **kwargs):
        print("before")
        func(*args, **kwargs)
        print("after")
    return nested

foo = decorator(foo)  # Manually decorate

@decorator
def bar():
    # Do something
    print("bar")
# bar() is decorated
```

## Reference or Copy ? 

/!\ les vues sont des références:
```
d = {'a':1,'b':2,'c':3}
k = d.keys()
print(k) # a,b,c
d['d'] = 4
print(k)  # a,b,c,d
```
Il est possible d'effectuer de vraies copies, soit en surface (que le 1er niveau) soit en profondeur via la module `copy`
```
l = [1,[2,[3]]]
l2 = l
import copy
l3 = copy.copy(l)
l4 = copy.deepcopy(l)
l[0]='spam'
l[1][1][0]='deep spam'
print('l  = ',l)
print('l2 = ',l2) # same as l
print('l3 = ',l2) # copy of first level, shared deeper levels
print('l4 = ',l4) # copy of all levels
```
NB:
* pour des listes, on peut créer une *shallow copy* avec un *slice*: `l5 = l[:]`
* les immutables ne sont pas vraiment copiés. La copie ne concerne que les mutables.

Pour s'y retrouver:
* `a is b` : indique si deux objets partagent la même mémoire
* `id(a)` : retourne l'identifiant de l'objet `a`

### Mutable in set: how to die painfully
```
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y    
    def __repr__(self):
        return f"Pointt[{self.x}, {self.y}]"
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    def __hash__(self):
        return (11 * self.x + self.y) // 16
t1, t2 = Point(10, 10), Point(10, 10)
s = {t1, t2}
s # {Point[10, 10]}
t1 in s, t2 in s # (True, True)
t1.x = 100
s # {Point[100, 10]}
t1 in s # False /!\ (le hachage n'a pas été recalculé /!\
```
Il est néanmoins possible de faire des objets non mutables:
```
from collections import namedtuple
class Point2(namedtuple('Point2', ['x', 'y'])):
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    def __hash__(self):
        return (11 * self.x + self.y) // 16
t1, t2 = Point2(10, 10), Point2(10, 10)
s = {t1, t2}
s # {Point2[10, 10]}
t1 in s, t2 in s # (True, True)
# t1.x = 100 # AttributeError
```
ou bien
```
from dataclasses import dataclass

@dataclass(frozen=True)
class Point3:
    x: float
    y: float

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return (11 * self.x + self.y) // 16

t1, t2 = Point3(10, 10), Point3(10, 10)
s = {t1, t2}
s # {Point3[10, 10]}
t1 in s, t2 in s # (True, True)
# t1.x = 100 # FrozenInstanceError
```

https://docs.python.org/3/reference/datamodel.html#object.__hash__
**If a class defines mutable objects and implements an __eq__() method, it should not implement __hash__(), since the implementation of hashable collections requires that a key’s hash value is immutable (if the object’s hash value changes, it will be in the wrong hash bucket).**


## La gestion des fichiers
```python
f = open(r'spam.txt', 'w', encoding= 'utf8')
for i in range(100):
    f.write(f"ligne {i+1}\n")
f.close()
!cat spam.txt
```

```python
with open(r'spam.txt', 'r', encoding= 'utf8') as f:
    for line in f:
        print(line)
with open(r'spam.bin', 'bw') as f:
    for i in range(100):
        f.write(b'\x3d')
```

Voir aussi le context manager
* https://realpython.com/python-with-statement/
* https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/courseware/39f73ecca8274b48b5edc06d00ab259c/1230630a77d5449da2d25df29cb01644/

# Bibliothèques pour le calcul scientifique

## Numpy
* https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/courseware/16fe81d60f134de9a10dd0c44a597584/d83dc146d29a450584f894b018c3f997/

* est-ce une vue ?
    ```python
    import numpy as np
    a = np.array([1,2,3,4], dtype='int32')
    b = a.reshape(2,2)
    print(a.flags['OWNDATA'])
    print(a.base)
    print(b.flags['OWNDATA'])
    print(b.base is a)
    ```

* Opérateur spéciaux:
  ```python
  import numpy as np
  A = np.matrix('3 1; 8 2')
  B = np.matrix('6 1; 7 9')
  A @ B
  ```

* Vectorisation multi-dimensionnelle
   ```python
   import numpy as np
   Xticks, Yticks = (np.linspace(-np.pi, np.pi, num=10),
                     np.linspace(3*np.pi, 5*np.pi, num=10))
   Z = np.cos(X) + np.cos(Y)**2
   from mpl_toolkits.mplot3d import Axes3D
   fig = plt.figure()
   ax = fig.add_subplot(111, projection='3d')
   ax.plot_wireframe(X, Y, Z);
   ```

* Vectorisation custom
    ```python
    import numpy as np
    a = np.linspace(-10, 10, num=10)
    
    # @np.vectorize # possible d'utiliser une directive pour vectoriser directement
    def scalar_function(x):
        return x**2 + 2*x + (1 if x <=0 else 10)
    
    vec_scalar_function = np.vectorize(scalar_function)
    
    %timeit [scalar_function(x) for x in a]
    %timeit vec_scalar_function(a)
    
    
    np.log.at(a, a>0)
    ```
  
* slice: Liste vs array
  ```python
  # Listes
  # une slice d'une liste est une shallow copy
  liste = [0, 1, 2]
  liste[1:2] # [1]
    
  # en modifiant la slice,
  # on ne modifie pas la liste
  liste[1:2][0] = 999999
  liste # [0, 1, 2]
    
  # Tableaux
  # une slice d'un tableau numpy est un extrait du tableau
  array = np.array([0, 1, 2])
  array[1:2] # array([1])
  array[1:2][0] = 100
  array # array([  0, 100,   2])
  ```

## Tracé avec matplotlib

```python
import numpy as np
import matplotlib.pyplot as plt
X = np.linspace(-10, 20)
Y = np.vectorize(f)(X)
plt.plot(X, Y)
plt.show()
```
