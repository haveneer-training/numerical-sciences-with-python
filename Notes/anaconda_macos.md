# Anaconda Ultimate

## to install anaconda

```
brew cask install anaconda
```
/!\ : `~/.conda` should be writable

## to clean anaconda:
```
conda install anaconda-clean
anaconda-clean --yes
rm -fr /Users/pascal/.anaconda_backup/*
brew uninstall anaconda
```