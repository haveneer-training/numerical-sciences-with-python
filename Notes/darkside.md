# Moderata sur le FUN MOOC Python & Fact checking
* langage de bas niveau (C++, Rust...)
  * oui, la gestion de la mémoire est au niveau du code utilisateur
  * non, **il est assez facile d'oublier de libérer la mémoire après usage**
    l'usage en C++≥11 de share_ptr ou unique_ptr ou des pointeurs standards en Rust font très bien le boulot
    pour une charge cognitive assez faible.
  * la technique du Garbage Collector (en Python par exemple), simplifie beaucoup la programmation
    mais rend moins fine la gestion de la mémoire et peut ralentir le code à des moments inattendus
  * **Dans un langage compilé avec typage statique, on doit fournir du typage** : 
  cela ne veut pas dire qu'il faut mettre des annotations de types partout (cf Rust, scala, C++≥11 est désormais est un langage à inférence de type avec `auto`)

* le typage dynamique induit souvent de violer le principe de Liskov (ou bien le permet trop facilement)
* en typage statique, beaucoup plus de contrôle de validité peuvent être faits pendant la phase de compilation (et non découverts à l'exécution "par hasard")
  * En développement logiciel, plus tard est trouvé un bug plus il sera *cher*.
* Typage statique en Python, par exemple avec http://www.mypy-lang.org

* Python mise **tout** sur la *simplicité* par rapport à la vitesse, la sécurité, la fiabilité...

* Le terme **simple** est employé à toutes les sauces, un peu comme une pensée partisane.

  La simplicité en Python n'est pas générale (ou c'est bien un biais idéologique)
  * références partagées qui auraient pu être CoW/les copies shallow ou deep
  * le non-typage (ou typage très laxiste, aka **hints** : on le met et ça ne fait rien)
  * la non-compilation avant exécution
  * les **leaks** de variables de boucles, mais pas tout le temps (sauf for-comprehensions)
  * la complexité inhérente à alors tester/valider le code
  * L'interprétation du code ne se produit qu'au moment de l'appel (/!\ pb de 'compilation' à ce moment là)
    idem lors du chargement des modules. Ça complique sensiblement le refactoring.
    ```
    def f():
        g() # n'exite pas
    # On ne sait pas si f() est valide
    f() # error:
    ```
  * ...    

* et parfois le terme **performance** ou **efficacité** ne doit pas faire oublier que le python est usuellement 10 à 100 fois plus lent que du C

# Bonus
## Divers
* module `argparse` : https://www.fun-mooc.fr/courses/course-v1:UCA+107001+session02/courseware/b4a443f89e7145d79355eee81c358718/1125c3331b9443a08e594ec41c796e2b/

* `getsizeof`: consommation mémoire d'un objet
  ```
  import sys
  getsizeof(x)
  ```

* La comparaison sur les listes est lexicographique

    cf https://docs.python.org/3/library/stdtypes.html#comparisons    
    **In particular, tuples and lists are compared lexicographically by comparing corresponding elements.**

* Auto-référencement infini:
```
infini_1=[None]
infini_1[0] = infini_1
print(infini_1)
```

* Si une classe change après avoir été instanciée, les précédentes instances peuvent ne pas être impactées par le changement

## Les implicites
* Les modifications *cachées* des itérateurs (après utilisation, ils sont vides et aucune mise en garde)
  
## Modules
* `itertools`: semble très bon : https://docs.python.org/3/library/itertools.html
* `functools`: autre qui semble pas mal : https://docs.python.org/3/library/functools.html
    
## Modification et introspection de scope
* force access to global variable : `global variable_name`
* `global`
* `nonlocal`
* `locals()` : liste les variables locales
* `globals()` : liste les variables locales
* La règle LEGB
    * https://realpython.com/python-scope-legb-rule/
    * https://www.geeksforgeeks.org/scope-resolution-in-python-legb-rule/

## Develop mode
* https://stackoverflow.com/questions/5677809/how-can-i-test-my-python-module-without-installing-it
* ```
  pip3 wheel ./Python
  python3 setup.py develop
  python3 setup.py build --debug
  export PYTHONPATH=./build/lib.macosx-10.15-x86_64-3.7 && python3 tests/test.py
  ```
* Must have:
  * mypy : analyse statique de code

* *duck typing* : If it walks like a duck, swims like a duck, and quacks like a duck, then it probably is a duck.
