import numpy as np


def QRdecomposition(A):
    # on réutilise les notations de la page: https://en.wikipedia.org/wiki/QR_decomposition
    m, n = A.shape
    Q = np.identity(m)
    R = A
    Ak = A
    for k in range(m - 1):
        # en suivant précisant l'algo de la doc
        # mk = Ak.shape[0]
        # xk = Ak[:,0].reshape(-1,1)
        # alphak = np.linalg.norm(xk, ord=2) #* np.sign(xk[0]) # sign trick for numerically more stable
        # ek=np.zeros((mk,1)); ek[0]=1
        # uk=xk-alphak*ek

        # petit raccourci...
        xk = Ak[:, 0].reshape(-1, 1)
        alphak = np.linalg.norm(xk, ord=2) * np.sign(xk[0])  # sign trick for numerically more stable
        uk = xk.copy()
        uk[0] -= alphak

        vk = uk / np.linalg.norm(uk, ord=2)
        Qk = np.identity(m)
        Qk[k:, k:] = Qk[k:, k:] - 2 * vk @ vk.transpose()

        # mise à jour des matrices de construction
        R = Qk @ R
        Ak = (Qk[k:, k:] @ Ak)[1:, 1:]
        Q = Q @ Qk

        # to manage non square matrix
        if Ak.size == 0:
            break

    # R = Q.transpose() @ A # construction alternative non itérative
    assert np.allclose(np.tril(R, k=-1), np.zeros(R.shape))
    R = np.triu(R)  # force R to be upper triangular
    return Q, R

