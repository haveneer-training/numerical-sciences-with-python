import numpy as np


def compute_regression_coefficients(x, y):
    mean_x, mean_y = np.mean(x), np.mean(y)

    # calculating cross-deviation and deviation about x

    # deux formulations possibles
    # SS_xy = np.sum(y*x - mean_y * mean_x)
    # SS_xx = np.sum(x*x - mean_x * mean_x)
    SS_xy = np.sum((y - mean_y) * (x - mean_x))
    SS_xx = np.sum((x - mean_x) ** 2)

    # compute regression coefficients
    # cf https://en.wikipedia.org/wiki/Simple_linear_regression
    beta = SS_xy / SS_xx
    alpha = mean_y - beta * mean_x

    return alpha, beta


def computeR2(x, y, y_pred):
    # computing the coefficient of determination (R2)
    # cf https://en.wikipedia.org/wiki/Coefficient_of_determination
    SS_res = sum([(yi - yi_pred) ** 2 for (yi, yi_pred) in zip(y, y_pred)])
    mean_y = np.mean(y)
    SS_tot = sum([(yi - mean_y) ** 2 for yi in y])
    R2 = 1 - SS_res / SS_tot
    return R2


class RandomDataError(Exception):
    pass


def generate_random_data(minv, maxv, size):
    if minv > maxv:
        raise RandomDataError("minv should be lower than maxv")
    x = np.random.rand(size) * (maxv - minv) + minv
    y = np.random.rand(size) * (maxv - minv) + minv
    return x, y
