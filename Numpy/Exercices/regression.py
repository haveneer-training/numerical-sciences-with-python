import numpy as np
from QRdecomposition import *
import linear_regression as lr
import matplotlib.pyplot as plt


def eval_poly(c, x):
    r = 0 * x
    for ci in c[::-1]:
        r *= x
        r += ci
    return r


def compute_r2(y, y_pred):
    # computing the coefficient of determination (R2)
    # cf https://en.wikipedia.org/wiki/Coefficient_of_determination
    SS_res = sum([(yi - yi_pred) ** 2 for (yi, yi_pred) in zip(y, y_pred)])
    mean_y = np.mean(y)
    SS_tot = sum([(yi - mean_y) ** 2 for yi in y])
    R2 = 1 - SS_res / SS_tot
    return R2


def compute_regression_coefficients(x, y, n):
    narray = np.arange(n + 1)  # choose your degree

    A = x.reshape(-1, 1) ** narray
    Q, R = QRdecomposition(A)
    # Q @ R @ c = y
    # (Q* @ Q) @ R @ c = Q* y
    # R is triangular => solve by elimination

    Qy = Q.T @ y.reshape(-1, 1)
    c = np.zeros(narray.size)
    for ci in narray[::-1]:  # reverse loop
        c[ci] = (Qy[ci] - np.sum(R[ci, ci + 1:narray.size] * c[ci + 1:narray.size])) / R[ci, ci]
    return c


def top_points():
    return [(3.7, 4.1), (4.2, 4.2), (5.3, 4.4)]


class RandomDataError(Exception):
    pass


def generate_random_data(minv, maxv, size):
    if minv > maxv:
        raise RandomDataError("minv should be lower than maxv")
    x = np.random.rand(size) * (maxv - minv) + minv
    y = np.random.rand(size) * (maxv - minv) + minv
    return x, y


if __name__ == '__main__':
    x = np.array([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
    y = np.array([5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])

    plt.scatter(x, y, color="red")

    alpha, beta = lr.compute_regression_coefficients(x, y)
    y_pred = alpha + beta * x
    R2 = lr.computeR2(x, y, y_pred)
    print(f"R2={R2:.3}")
    plt.plot(x, y_pred, color="green")

    n = np.arange(3)  # choose your degree

    A = x.reshape(-1, 1) ** n
    Q, R = QRdecomposition(A)
    # Q @ R @ c = y
    # (Q* @ Q) @ R @ c = Q* y
    # R is triangular => solve by elimination

    Qy = Q.T @ y.reshape(-1, 1)
    c = np.zeros(n.size)
    for ci in n[::-1]:  # reverse loop
        c[ci] = (Qy[ci] - np.sum(R[ci, ci + 1:n.size] * c[ci + 1:n.size])) / R[ci, ci]

    y_pred2 = np.sum(c * x.reshape(-1, 1) ** n, axis=1)
    R2 = lr.computeR2(x, y, y_pred2)
    print(f"R2={R2:.3}")
    plt.plot(x, y_pred2, color="blue")

    plt.show()
