import numpy as np


def check_values(grid):
    return check_subgrid(grid)


def check_lines(grid):
    for i in range(9):
        if not check_subgrid(grid[i, :]):
            return False
    return True


def check_columns(grid):
    return check_lines(grid.T)


def check_squares(grid):
    for i in range(3):
        for j in range(3):
            if not check_subgrid(grid[(3 * i):(3 * (i + 1)), (3 * j):(3 * (j + 1))]):
                return False
    return True


def check_diags(grid):
    return check_subgrid(np.diag(grid)) and check_subgrid(np.diag(np.rot90(grid)))  # rot90 or fliplr or flipup


def check_subgrid(sg):
    return set(sg.ravel()) == set(range(1, 10))
