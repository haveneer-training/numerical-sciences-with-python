import numpy as np
import pytest
from QRdecomposition import *


def gen_param():
    m = np.random.randint(5, 50)
    n = np.random.randint(5, 50)
    return m, n


@pytest.mark.parametrize('m,n', [gen_param() for _ in range(20)])
def test_random(m, n):
    for _ in range(10):
        A = np.random.rand(m, n)
        Q, R = QRdecomposition(A)
        assert np.allclose(np.identity(Q.shape[0]), Q.T @ Q)
        assert np.allclose(A, Q @ R)


def test_demo():
    # Wikipedia example
    A = np.array([[12, -51, 4], [6, 167, -68], [-4, 24, -41]], dtype='float64')
    Q, R = QRdecomposition(A)
    print(f"\nQ=\n{Q}\nR={R}") # to print this, use: --capture=tee-sys
    assert np.allclose(np.identity(Q.shape[0]), Q.T @ Q)
    assert np.allclose(A, Q @ R)

def test_demo2():
    # http://www.klubprepa.fr/Site/Document/ChargementDocument.aspx?IdDocument=3658
    A = np.array([[-4, 20, 35, 5], [-4 , -30, -15, 55], [-8, 40, -80, -65], [23, -15, 30, 15]], dtype='float64')
    Q, R = QRdecomposition(A)
    print(f"\nQ=\n{Q}\nR={R}") # to print this, use: --capture=tee-sys
    assert np.allclose(np.identity(Q.shape[0]), Q.T @ Q)
    assert np.allclose(A, Q @ R)
