import pytest
import numpy as np
from linear_regression import *


def test_data():
    x = np.array([8.2, 7, 6.5, 5.3, 5.2, 4, 4.6, 4.5, 4.2, 3.7])
    y = np.array([5, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])

    print(x.shape)
    print(y.shape)

    # compute coefficients
    alpha, beta = compute_regression_coefficients(x, y)
    assert alpha == pytest.approx(3.3086263963591316)
    assert beta == pytest.approx(0.2013860157219679)

    print("Estimated coefficients:\nalpha = {} \nbeta = {}".format(alpha, beta))

    # compute predicted response
    y_pred = alpha + beta * x

    R2 = computeR2(x, y, y_pred)

    print(f"R^2 = {R2}")
    assert R2 == pytest.approx(0.9851722929916441)


def test_data2():
    x = np.array([0.30363948, 0.25851756, 0.86409702, 0.74509113, 0.92580452, 0.38798998,
                   0.9876579, 0.29634637, 0.27051846, 0.94772089, 0.35299552, 0.57216634])
    y = np.array([0.98029043, 0.90450999, 0.32225616, 0.66615898, 0.72523267, 0.59951528,
                  0.63077971, 0.62735048, 0.75914951, 0.40147196, 0.16787586, 0.0078236])

    # compute coefficients
    alpha, beta = compute_regression_coefficients(x, y)
    assert alpha == pytest.approx(0.7104163699910486)
    assert beta == pytest.approx(-0.2506431086211018)

    print("Estimated coefficients:\nalpha = {} \nbeta = {}".format(alpha, beta))

    # compute predicted response
    y_pred = alpha + beta * x

    R2 = computeR2(x, y, y_pred)

    print(f"R^2 = {R2}")
    assert R2 == pytest.approx(0.06612703616085869)


def test_random_data():
    import random
    for _ in range(10):
        minv = random.randint(-100, 100)
        maxv = random.randint(minv, 100)
        size = random.randint(1, 100)
        x, y = generate_random_data(minv, maxv, size)


def test_exception():
    e = RandomDataError("aaa")
    assert isinstance(e, Exception)


def test_random_data_flat():
    np.all(generate_random_data(10, 10, 100) == 10)


def test_random_data_bad():
    with pytest.raises(RandomDataError):
        generate_random_data(10, 9, 1)


if __name__ == '__main__':
    test_data()
