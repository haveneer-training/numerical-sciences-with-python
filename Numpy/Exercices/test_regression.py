import pytest
import numpy as np
from regression import *


def test_eval_poly0():
    # assert eval_poly(np.array([1, 2, 3]), 0) == pytest.approx(1)
    assert np.allclose(eval_poly(np.array([1, 2, 3]), 0), 1)


def test_eval_poly1():
    # assert eval_poly(np.array([1, 2, -3]), 1) == pytest.approx(0)
    assert np.allclose(eval_poly(np.array([1, 2, -3]), 1), 0)


def test_eval_poly2():
    # assert eval_poly(np.array([1, 2, 3]), 2) == pytest.approx(17)
    assert np.allclose(eval_poly(np.array([1, 2, 3]), 2), 17)


def test_eval_poly3():
    # assert eval_poly(np.array([1, 2, 3]), np.array([0, 1, 2])) == pytest.approx(np.array([1, 6, 17]))
    assert np.allclose(eval_poly(np.array([1, 2, 3]), np.array([0, 1, 2])), np.array([1, 6, 17]))


def test_data_degree0():
    x = np.array([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
    y = np.array([5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])

    # compute coefficients
    c = compute_regression_coefficients(x, y, 0)
    assert c.size == 1
    # assert c == pytest.approx([4.38])
    assert np.allclose(c, [4.38])

    # compute predicted response
    y_pred = eval_poly(c, x)

    R2 = compute_r2(y, y_pred)

    print(f"R^2 = {R2}")
    # assert R2 == pytest.approx(1.1102230246251565e-16)
    assert np.allclose(R2, 1.1102230246251565e-16)


def test_data_degree1():
    x = np.array([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
    y = np.array([5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])

    # compute coefficients
    c = compute_regression_coefficients(x, y, 1)
    assert c.size == 2
    # assert c == pytest.approx([3.3086263963591316, 0.2013860157219679])
    assert np.allclose(c, [3.3086263963591316, 0.2013860157219679])

    # compute predicted response
    y_pred = eval_poly(c, x)

    R2 = compute_r2(y, y_pred)

    print(f"R^2 = {R2}")
    # assert R2 == pytest.approx(0.9851722929916441)
    assert np.allclose(R2, 0.9851722929916441)


def test_data_degree2():
    x = np.array([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
    y = np.array([5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])

    # compute coefficients
    c = compute_regression_coefficients(x, y, 2)
    assert c.size == 3
    # assert c == pytest.approx([3.7194292, 0.05177981, 0.01273655])
    assert np.allclose(c, [3.7194292, 0.05177981, 0.01273655])

    # compute predicted response
    y_pred = eval_poly(c, x)

    R2 = compute_r2(y, y_pred)

    print(f"R^2 = {R2}")
    # assert R2 == pytest.approx(0.9912596176710542)
    assert np.allclose(R2, 0.9912596176710542)


def test_data2_degree1():
    x = np.array([0.30363948, 0.25851756, 0.86409702, 0.74509113, 0.92580452, 0.38798998,
                  0.9876579, 0.29634637, 0.27051846, 0.94772089, 0.35299552, 0.57216634])
    y = np.array([0.98029043, 0.90450999, 0.32225616, 0.66615898, 0.72523267, 0.59951528,
                  0.63077971, 0.62735048, 0.75914951, 0.40147196, 0.16787586, 0.0078236])

    # compute coefficients
    c = compute_regression_coefficients(x, y, 1)
    # assert c == pytest.approx([0.71041637, -0.25064311])
    assert np.allclose(c, [0.71041637, -0.25064311])

    # compute predicted response
    y_pred = eval_poly(c, x)

    R2 = compute_r2(y, y_pred)

    print(f"R^2 = {R2}")
    # assert R2 == pytest.approx(0.06612703616085869)
    assert np.allclose(R2, 0.06612703616085869)


def test_data2_degree4():
    x = np.array([0.30363948, 0.25851756, 0.86409702, 0.74509113, 0.92580452, 0.38798998,
                  0.9876579, 0.29634637, 0.27051846, 0.94772089, 0.35299552, 0.57216634])
    y = np.array([0.98029043, 0.90450999, 0.32225616, 0.66615898, 0.72523267, 0.59951528,
                  0.63077971, 0.62735048, 0.75914951, 0.40147196, 0.16787586, 0.0078236])

    # compute coefficients
    c = compute_regression_coefficients(x, y, 4)
    # assert c == pytest.approx([3.91278658, -17.70553669, 27.30472261, -14.07490272, 1.10575573])
    assert np.allclose(c, [3.91278658, -17.70553669, 27.30472261, -14.07490272, 1.10575573])

    # compute predicted response
    y_pred = eval_poly(c, x)

    R2 = compute_r2(y, y_pred)

    print(f"R^2 = {R2}")
    # assert R2 == pytest.approx(0.48928224473121606)
    assert np.allclose(R2, 0.48928224473121606)


def test_top_points():
    assert np.array_equal(top_points(), [(3.7, 4.1), (4.2, 4.2), (5.3, 4.4)])


def test_random_data():
    import random
    for _ in range(10):
        minv = random.randint(-100, 100)
        maxv = random.randint(minv, 100)
        size = random.randint(1, 100)
        x, y = generate_random_data(minv, maxv, size)


def test_exception():
    e = RandomDataError("aaa")
    assert isinstance(e, Exception)


def test_random_data_flat():
    np.all(generate_random_data(10, 10, 100) == 10)


def test_random_data_bad():
    with pytest.raises(RandomDataError):
        generate_random_data(10, 9, 1)


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    x = np.array([8.2, 7.0, 6.5, 5.3, 5.2, 4.0, 4.6, 4.5, 4.2, 3.7])
    y = np.array([5.0, 4.7, 4.6, 4.4, 4.3, 4.1, 4.2, 4.2, 4.2, 4.1])

    plt.scatter(x, y, color="red")

    c1 = compute_regression_coefficients(x, y, 1)
    print("Estimated coefficients:\nc = {}".format(c1))

    # y_pred2 = np.sum(c1 * x.reshape(-1, 1) ** narray, axis=1)
    y_pred1 = eval_poly(c1, x)
    R2 = compute_r2(y, y_pred1)
    print(f"R2={R2:.3}")
    plt.plot(x, y_pred1, color="blue")

    c2 = compute_regression_coefficients(x, y, 2)
    print("Estimated coefficients:\nc = {}".format(c1))

    # y_pred2 = np.sum(c2 * x.reshape(-1, 1) ** narray, axis=1)
    y_pred2 = eval_poly(c2, x)
    R2 = compute_r2(y, y_pred2)
    print(f"R2={R2:.3}")
    plt.plot(x, y_pred2, color="green")

    plt.show()
