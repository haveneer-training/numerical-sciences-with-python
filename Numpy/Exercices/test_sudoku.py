import pytest
from sudoku import *
import numpy as np

good_grid1 = np.array([
    [3, 1, 6, 5, 7, 8, 4, 9, 2],
    [5, 2, 9, 1, 3, 4, 7, 6, 8],
    [4, 8, 7, 6, 2, 9, 5, 3, 1],
    [2, 6, 3, 4, 1, 5, 9, 8, 7],
    [9, 7, 4, 8, 6, 3, 1, 2, 5],
    [8, 5, 1, 7, 9, 2, 6, 4, 3],
    [1, 3, 8, 9, 4, 7, 2, 5, 6],
    [6, 9, 2, 3, 5, 1, 8, 7, 4],
    [7, 4, 5, 2, 8, 6, 3, 1, 9]])

good_grid2 = np.array([
    [8, 2, 5, 1, 6, 9, 7, 4, 3],
    [4, 7, 6, 5, 2, 3, 1, 8, 9],
    [1, 9, 3, 4, 8, 7, 6, 2, 5],
    [9, 5, 2, 6, 1, 8, 3, 7, 4],
    [7, 3, 4, 9, 5, 2, 8, 6, 1],
    [6, 1, 8, 3, 7, 4, 9, 5, 2],
    [3, 6, 7, 2, 4, 1, 5, 9, 8],
    [2, 8, 1, 7, 9, 5, 4, 3, 6],
    [5, 4, 9, 8, 3, 6, 2, 1, 7]])

good_grid3 = np.array([
    [5, 8, 1, 4, 2, 7, 6, 9, 3],
    [3, 7, 4, 5, 9, 6, 8, 1, 2],
    [9, 6, 2, 1, 3, 8, 4, 7, 5],
    [6, 2, 9, 3, 8, 5, 7, 4, 1],
    [1, 5, 7, 9, 6, 4, 3, 2, 8],
    [8, 4, 3, 2, 7, 1, 5, 6, 9],
    [4, 1, 8, 7, 5, 2, 9, 3, 6],
    [2, 9, 5, 6, 4, 3, 1, 8, 7],
    [7, 3, 6, 8, 1, 9, 2, 5, 4]])

bad_grid0 = np.array([
    [3, 1, 6, 5, 7, 8, 4, 9, 2],
    [5, 2, 9, 1, 3, 4, 7, 6, 8],
    [4, 8, 7, 6, 2, 9, 5, 3, 1],
    [2, 6, 0, 4, 1, 5, 9, 8, 7],
    [9, 7, 4, 8, 6, 3, 1, 2, 5],
    [8, 5, 1, 7, 9, 2, 6, 4, 3],
    [1, 3, 8, 9, 4, 7, 2, 5, 6],
    [6, 9, 2, 3, 5, 1, 0, 7, 4],
    [7, 4, 5, 2, 8, 6, 3, 1, 9]])

bad_grid1 = np.array([
    [3, 1, 6, 5, 7, 8, 4, 9, 2],
    [5, 2, 8, 1, 3, 4, 7, 6, 8],
    [4, 8, 7, 6, 2, 9, 5, 3, 1],
    [2, 6, 3, 4, 1, 5, 9, 8, 7],
    [9, 7, 4, 8, 6, 3, 1, 2, 5],
    [8, 5, 1, 7, 9, 2, 6, 4, 3],
    [1, 3, 8, 9, 4, 7, 2, 5, 6],
    [6, 9, 2, 3, 5, 1, 8, 7, 4],
    [7, 4, 5, 2, 8, 6, 3, 1, 9]])

bad_grid2 = np.array([
    [8, 2, 1, 5, 6, 9, 7, 4, 3],
    [4, 7, 5, 6, 2, 3, 1, 8, 9],
    [1, 9, 4, 3, 8, 7, 6, 2, 5],
    [9, 5, 6, 2, 1, 8, 3, 7, 4],
    [7, 3, 9, 4, 5, 2, 8, 6, 1],
    [6, 1, 3, 8, 7, 4, 9, 5, 2],
    [3, 6, 2, 7, 4, 1, 5, 9, 8],
    [2, 8, 7, 1, 9, 5, 4, 3, 6],
    [5, 4, 8, 9, 3, 6, 2, 1, 7]])

bad_grid3 = np.array([
    [6, 3, 5, 7, 8, 2, 9, 1, 4],
    [2, 7, 4, 5, 9, 1, 6, 8, 3],
    [9, 1, 8, 3, 4, 6, 5, 2, 7],
    [7, 5, 2, 6, 1, 9, 4, 3, 8],
    [1, 8, 9, 4, 3, 5, 2, 7, 6],
    [3, 6, 4, 8, 2, 7, 1, 9, 5],
    [4, 9, 6, 1, 7, 8, 2, 5, 3],
    [8, 2, 3, 9, 5, 4, 7, 6, 1],
    [5, 7, 1, 2, 6, 3, 8, 4, 9]])


def test_values():
    assert check_values(good_grid1) is True
    assert check_values(good_grid2) is True
    assert check_values(good_grid3) is True
    assert check_values(bad_grid0) is False
    assert check_values(bad_grid1) is True
    assert check_values(bad_grid2) is True
    assert check_values(bad_grid3) is True


def test_lines():
    assert check_lines(good_grid1) is True
    assert check_lines(good_grid2) is True
    assert check_lines(good_grid3) is True
    assert check_lines(bad_grid0) is False
    assert check_lines(bad_grid1) is False
    assert check_lines(bad_grid2) is True
    assert check_lines(bad_grid3) is True


def test_columns():
    assert check_columns(good_grid1) is True
    assert check_columns(good_grid2) is True
    assert check_columns(good_grid3) is True
    assert check_columns(bad_grid0) is False
    assert check_columns(bad_grid1) is False
    assert check_columns(bad_grid2) is True
    assert check_columns(bad_grid3) is False


def test_squares():
    assert check_squares(good_grid1) is True
    assert check_squares(good_grid2) is True
    assert check_squares(good_grid3) is True
    assert check_squares(bad_grid0) is False
    assert check_squares(bad_grid1) is False
    assert check_squares(bad_grid2) is False
    assert check_squares(bad_grid3) is True


def test_diags():
    assert check_diags(good_grid1) is False
    assert check_diags(good_grid2) is False
    assert check_diags(good_grid3) is True
    assert check_diags(bad_grid0) is False
    assert check_diags(bad_grid1) is False
    assert check_diags(bad_grid2) is False
    assert check_diags(bad_grid3) is False
