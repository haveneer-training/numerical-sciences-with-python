﻿# Calculons avec Numpy

## Consignes générales

- Tout texte écrit avec une `police à pas fixe` doit être repris littéralement soit dans votre code soit en tant que
  commande. Tout texte écrit avec une *police en italique* demande une adaptation de votre part (adaptation qui peut
  être à interpréter en fonction du contexte).
- La version minimum de Python requise est la 3.7. Vous pouvez vérifier votre version grâce à la commande `python -V`
- Nous appliquerons les règles de développement de Python décrit
  dans [PEP8 standards](https://www.python.org/dev/peps/pep-0008/). Pour vous y aider, configurez votre éditeur en
  conséquence. Vous pouvez aussi installer [pycodestyle](https://pypi.org/project/pycodestyle) qui permet de vérifier la
  conformité de votre code avec ces règles.
- La structure même du cours ne peut couvrir toutes les situations que vous rencontrerez dans votre parcours. Le cours
  est avant un tremplin vous exposant les lignes directrices et les particularités moins évidentes. Python est un
  langage populaire et il existe beaucoup de ressources disponibles sur internet. N’hésitez jamais à y avoir recours (
  c’est d’ailleurs une pratique requérant aussi de l'entraînement au niveau du vocabulaire, de la pratique récurrente de
  l’anglais et l’analyse critique des *réponses* que vous y trouverez). Ne recopiez jamais du code sans comprendre ce
  que vous faîtes. Certaines commandes ou certains scripts pourraient tout simplement endommager votre système.

## Exercice 1 : Factorisation QR d’une matrice par la méthode de Householder

L’objectif est de factoriser une matrice donnée *A* de taille *m* x *n* (*m* lignes, *n* colonnes) sous la forme d’un
produit d’une matrice unitaire[^1] Q de taille *m* x *m* et d’une matrice R de taille *m* x *n*.

![](Images/16ff838c-8e15-4819-b003-6588a00ddf9d.001.png)

1. Vous utiliserez la méthode de Householder[^2] pour écrire une fonction `QRdecomposition` qui prend une matrice en
   entrée et retourne les deux matrices Q et R. Toutes ces matrices sont de `numpy.array`.
1. Vous vérifierez que le produit des deux matrices Q et R obtenus est bien égal à la matrice initiale A.
1. Utilisez[^3] la décomposition QR pour résoudre un système linéaire de type Ax=b avec A une matrice carrée de taille n
   x n et x,b deux vecteurs de taille n (la matrice et les vecteurs sont des `numpy.array`). Comparez en précision et en
   vitesse votre solution obtenue avec celle produite par `numpy` pour le même système.

## Exercice 2 : Approximation par la méthode des moindres carrés

Nous supposons que nous disposons de *m* couples de données (x1, y1)...(xm,ym) (i≠j ⇒ xi≠xj). On cherche à construire
une approximation polynomiale *P* qui soit la meilleure au sens des moindres carrés:

P(X) = c0 + c1X+...+cm-1Xn-1 où n≤m.

Si n=m, on parle d’interpolation polynomiale. Nous n’allons ici considérer que des cas où n < m. Les coefficients de P
vérifient alors le système (surdéterminé) de *m* équations à *n* inconnues.

![](Images/16ff838c-8e15-4819-b003-6588a00ddf9d.002.png)![](Images/16ff838c-8e15-4819-b003-6588a00ddf9d.003.png)

Résoudre ce système surdéterminé revient à rendre le plus petit possible le résidu Ax-b au sens de la norme euclidienne
||.||2. Il est possible de montrer que « Trouver *x* qui minimise ||Ax-b||22 = (Ax-b)*.(Ax-b) » possède au moins une
solution. L’ensemble des solutions coïncident avec celui du système A*Ax=A*b.

Par ailleurs, si x1 et x2 sont des solutions de ce système, alors A x_1 = A x_2.

Enfin, si la matrice A*A est régulière (i.e. de rang plein) alors la solution est unique.

1. Utilisez la factorisation QR de l’exercice précédent pour résoudre un problème aux moindres carrés.
1. Testez cette méthode sur les données[^4] issues
   de [Spurious Correlations](https://www.tylervigen.com/spurious-correlations):

  ```
  columns, Margarine consumed (lbs), Divorce rate in Maine (/1000)
  2000,8.2,5
  2001,7,4.7
  2002,6.5,4.6
  2003,5.3,4.4
  2004,5.2,4.3
  2005,4,4.1
  2006,4.6,4.2
  2007,4.5,4.2
  2008,4.2,4.2
  2009,3.7,4.1
  ```

pour calculer une approximation par un polynôme de degré 1 (n=2) puis de degré 2 (n=3).

Que peut-on dire du niveau d’approximation de ces deux approximations par rapport à celle obtenue avec la régression
linéaire ?

# Exercice 3 : vérificateur de Sudoku
Fichier `sudoku.py`

Soit une grille de Sodoku[^5] représentée par un tableau numpy 2D de taille 9x9 dont les valeurs sont des nombres de 1 à 9.

En prenant une telle grille en entrée, écrivez:

- une fonction `check_values` qui vérifie que toutes les valeurs présentes sont des entiers entre 1 et 9
- une fonction `check_lines` qui vérifie si toutes les lignes sont correctement remplies 
- une fonction `check_columns` qui vérifie si toutes les colonnes sont correctement remplies 
- une fonction `check_squares` qui vérifie si tous les sous-carrés 3x3 sont correctement remplis ![](Images/e9170f93-2108-4c29-9d93-fd6fec4fd64b.001.png)
- une fonction `check_diags` qui vérifie si toutes deux grandes diagonales correctement remplies.




[^1]: <https://fr.wikipedia.org/wiki/Matrice_unitaire> : A*A = I avec A* la matrice adjointe de A.
[^2]: [https://fr.wikipedia.org/wiki/Décomposition_QR](https://fr.wikipedia.org/wiki/D%C3%A9composition_QR)
, <https://en.wikipedia.org/wiki/QR_decomposition>
[^3]: <https://en.wikipedia.org/wiki/QR_decomposition#Using_for_solution_to_linear_inverse_problems>
[^4]: Données
originales: [National Vital Statistics Reports](https://www.cdc.gov/nchs/data/dvs/state-divorce-rates-90-95-99-18.pdf)
set [U.S. Department of Agriculture](https://www2.census.gov/library/publications/2011/compendia/statab/131ed/tables/12s0217.xls)
[^5]: <https://fr.wikipedia.org/wiki/Sudoku> 

