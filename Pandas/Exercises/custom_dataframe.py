import pandas as pd
import numpy as np


def get_dataframe():
    df = pd.DataFrame({'X': np.arange(1, 10),
                       'Y': np.arange(1009, 1000, -1),
                       'Z': np.arange(10001, 10028, 3),
                       'row': list("ABCDEFGHI")})
    df = df.set_index("row")
    return df


if __name__ == '__main__':
    get_dataframe()
