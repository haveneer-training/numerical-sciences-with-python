import numpy as np
import pandas as pd


def load_data():
    # return pd.read_csv('https://www.data.gouv.fr/fr/datasets/r/bc42c2e3-d24c-4499-a966-d35656c6cfc1',
    #                    compression='zip', header=0, sep='[ ]?;', quotechar='"', encoding='CP1250', engine='python')
    return pd.read_csv('https://github.com/haveneer/python-training-data/raw/master/extrait_de_pollution_des_vehicules_en_france.zip',
                       compression='zip')


def get_energy_count(df):
    return df['energ'].value_counts().count()


def get_gazoil_count(df):
    return (df.energ == 'GO').sum()


def get_max_power(df):
    return df['puiss_max'].agg('max')


def get_max_power_car(df):
    return df[df.puiss_max == df['puiss_max'].agg('max')].cnit


def get_cleanest_constructor(df):
    return df.groupby('mrq_utac').co2_mixte.mean().nsmallest(5).index


def get_lowest_taxe_ratio(df):
    df['ratio'] = df.co2_mixte / df.puiss_admin
    return set(df.nsmallest(50, columns='ratio').mrq_utac)


def get_cleanest_energy_top(df):
    return df.groupby('energ').co2_mixte.median().nsmallest(10).sort_values().index
