from custom_dataframe import *

def test_dataframe():
    df = get_dataframe()
    import hashlib
    df_hash = hashlib.sha256(df.to_json().encode()).hexdigest()
    assert df_hash == "59af1f91d54921988bee86fbdcc54deea2b88d1feafe4f365ec95ccbdaec706f"


def test_dataframe_print():
    df = get_dataframe()
    assert str(df) == (
        '     X     Y      Z\n'
        'row                \n'
        'A    1  1009  10001\n'
        'B    2  1008  10004\n'
        'C    3  1007  10007\n'
        'D    4  1006  10010\n'
        'E    5  1005  10013\n'
        'F    6  1004  10016\n'
        'G    7  1003  10019\n'
        'H    8  1002  10022\n'
        'I    9  1001  10025')
