import pytest
from pollution import *
import numpy as np
import pandas as pd

# df = pd.read_csv('https://www.data.gouv.fr/fr/datasets/r/bc42c2e3-d24c-4499-a966-d35656c6cfc1',
#                  compression='zip', header=0, sep='[ ]?;', quotechar='"', encoding='CP1250', engine="python")
# données simplifiée via
# ```
# df2 = df.sample(2000)
# df2.to_csv("extrait_de_pollution_des_vehicules_en_france.csv")
# ```

df = pd.read_csv(
    'https://github.com/haveneer/python-training-data/raw/master/extrait_de_pollution_des_vehicules_en_france.zip',
    compression='zip')

modified_df = df.iloc[::2]


def test_load_data():
    df_tmp = load_data()
    import hashlib

    df_hash = hashlib.sha256(df.to_json().encode()).hexdigest()
    df_tmp_hash = hashlib.sha256(df_tmp.to_json().encode()).hexdigest()

    assert df_hash == "e66373eda8e1bb0f7353a5a1cf5a8077a60f32bcb0a22d68bdf8fdf53c343734"
    assert df_tmp_hash == df_hash


def test_energy_count():
    assert get_energy_count(df) == 9


def test_gazoil_count():
    assert get_gazoil_count(df) == 1218


def test_max_power():
    assert get_max_power(df) == 585


def test_max_power_car():
    assert set(get_max_power_car(df)) == {'M10FERVP0007163'}


def test_cleanest_constructor():
    assert np.array_equal(get_cleanest_constructor(df), np.array(['SMART', 'HYUNDAI', 'SKODA', 'ALFA ROMEO', 'DACIA']))


def test_lowest_taxe_ratio():
    assert get_lowest_taxe_ratio(df) == {'MERCEDES', 'BMW', 'PORSCHE', 'FERRARI'}


def test_cleanest_energy_top():
    assert np.array_equal(get_cleanest_energy_top(df), ['EE', 'GH', 'GN/ES', 'EH', 'GO', 'ES/GN', 'ES', 'GN', 'EL'])
