import pytest
import numpy as np
import pandas as pd
from titanic import *

df = pd.read_csv('https://web.stanford.edu/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv')

modified_df = df.iloc[::2]


def test_load_data():
    df_tmp = load_data()
    import hashlib

    df_hash = hashlib.sha256(df.to_json().encode()).hexdigest()
    df_tmp_hash = hashlib.sha256(df_tmp.to_json().encode()).hexdigest()

    assert df_hash == "aa9471c4b139df52c886257fae115f348d1d533fd15a77e7ac9ca324bf88cb3a"
    assert df_tmp_hash == df_hash


def test_survivor_count():
    assert get_survivor_count(df) == 342


def test_survivor_count2():
    assert get_survivor_count(modified_df) == 168


def test_total_fare():
    assert get_total_fare(df) == "$28654.91"


def test_fare_count():
    assert get_fare_count(df) == 248


def test_fare_count2():
    assert get_fare_count(modified_df) == 183


def test_total_fare2():
    assert get_total_fare(modified_df) == "$13296.55"


def test_survivor_refund():
    assert get_survivor_refund(df) == "$16551.23"


def test_survivor_refund2():
    assert get_survivor_refund(modified_df) == "$7528.85"


def test_survivor_custom_refund():
    assert (get_survivor_custom_refund(df) == "$70770.00")


def test_survivor_custom_refund2():
    assert (get_survivor_custom_refund(modified_df) == "$31130.28")


def test_round_percentage():
    assert round_percentage(0) == 0
    assert round_percentage(0.01234) == 1.23
    assert round_percentage(0.12345) == 12.35
    assert round_percentage(1) == 100


def test_survival_ratio():
    assert get_survival_ratio(df) == 38.56


def test_survival_ratio_by_sex():
    assert get_survival_ratio_by_sex(df) == (74.2, 19.02)


def test_survival_ratio_by_sex_and_class():
    assert np.array_equal(get_survival_ratio_by_sex_and_class(df), [[96.81, 92.11, 50.], [36.89, 15.74, 13.7]])


def test_survival_ratio_by_sex_and_class2():
    assert np.array_equal(get_survival_ratio_by_sex_and_class(modified_df),
                          [[97.44, 88.57, 50.], [40.98, 15.69, 15.38]])


def test_survival_ratio_for_3rd_child():
    assert get_survival_ratio_for_3rd_child(df) == 41.51


def test_survival_ratio_for_3rd_child2():
    assert get_survival_ratio_for_3rd_child(modified_df) == 39.29


def test_oldest_survivor():
    assert np.array_equal(get_oldest_survivor(df), ['Mr. Algernon Henry Wilson Barkworth'])
