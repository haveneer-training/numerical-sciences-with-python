import numpy as np
import pandas as pd


def load_data():
    return pd.read_csv('https://web.stanford.edu/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv')


def get_survivor_count(df):
    return df.Survived.sum()


def round_amount(x):
    return f"${x:.2f}"


def get_total_fare(df):
    return round_amount(df.Fare.sum())


def get_fare_count(df):
    # return df.Fare.unique().size
    return df.Fare.value_counts().count()


def get_survivor_refund(df):
    return round_amount((df.Survived * df.Fare).sum())


def get_survivor_custom_refund(df):
    return round_amount(
        5 * df.Fare[df.Survived & (df.Pclass == 1)].sum()
        + 3 * df.Fare[df.Survived & (df.Pclass == 2)].sum()
    )


def round_percentage(x):
    # return round(x*100,2)
    return float(f"{100 * x:.2f}")


def get_survival_ratio(df):
    return round_percentage(df.Survived.mean())


def get_survival_ratio_by_sex(df):
    stat = df.groupby('Sex')['Survived'].mean()
    return round_percentage(stat.female), round_percentage(stat.male)


def get_survival_ratio_by_sex_and_class(df):
    stat = df.groupby(['Sex', 'Pclass'])['Survived'].mean()
    stat = np.array([round_percentage(x) for x in stat])
    return stat.reshape(2, 3)


def get_survival_ratio_for_3rd_child(df):
    return round_percentage(df[(df.Age < 11) & (df.Pclass == 3)].Survived.mean())


def get_oldest_survivor(df):
    # df.Name[np.argmax(df.Age)]
    return df[df['Survived'] == 1].nlargest(1, columns='Age').Name
