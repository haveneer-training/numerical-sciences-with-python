﻿# Explorons des données avec Pandas

## Consignes générales

- Tout texte écrit avec une `police à pas fixe` doit être repris littéralement soit dans votre code soit en tant que
  commande. Tout texte écrit avec une *police en italique* demande une adaptation de votre part (adaptation qui peut
  être à interpréter en fonction du contexte).
- La version minimum de Python requise est la 3.7. Vous pouvez vérifier votre version grâce à la commande `python -V`
- Nous appliquerons les règles de développement de Python décrit
  dans [PEP8 standards](https://www.python.org/dev/peps/pep-0008/). Pour vous y aider, configurez votre éditeur en
  conséquence. Vous pouvez aussi installer [pycodestyle](https://pypi.org/project/pycodestyle) qui permet de vérifier la
  conformité de votre code avec ces règles.
- La structure même du cours ne peut couvrir toutes les situations que vous rencontrerez dans votre parcours. Le cours
  est avant un tremplin vous exposant les lignes directrices et les particularités moins évidentes. Python est un
  langage populaire et il existe beaucoup de ressources disponibles sur internet. N’hésitez jamais à y avoir recours (
  c’est d’ailleurs une pratique requérant aussi de l'entraînement au niveau du vocabulaire, de la pratique récurrente de
  l’anglais et l’analyse critique des *réponses* que vous y trouverez). Ne recopiez jamais du code sans comprendre ce
  que vous faîtes. Certaines commandes ou certains scripts pourraient tout simplement endommager votre système.

## Exercice 0 : un dataframe à la main

Fichier `custom_datafram.py`

- Créez une fonction `get_dataframe()` qui retourne un Dataframe (de `pandas`) correspondant exactement à celui ci-dessous:

![](Images/custom_dataframe.png)

La correction automatique utile une comparaison via la fonction d’affichage print().

## Exercice 1 : comment s’*occupent*-ils ?

Fichier `what_are_you_doing.ipynb`

En utilisant les données de <https://raw.githubusercontent.com/justmarkham/DAT8/master/data/u.user>, répondez aux questions:
- Combien y a-t-il d’observations ?
- Quels sont les attributs (colonnes) disponibles ?
- Combien y a-t-il d’*occupations* différentes dans ce jeu de données ? Quelle est la plus courante ?
- Par classe d’âge (intervalle de 10 ans), donnez l’*occupation* la plus courante et la moins courante.
- Refaites ce calcul de distribution en y ajoutant une différenciation du genre. Cela change-t-il beaucoup les résultats ?
- Donnez l’âge médian par occupation et par genre.
- Y a-t-il des occupations où un genre est dominant (>80%) ?

## Exercice 2: Pollution des véhicules

Fichiers `pollution.py` et `pollution.ipynb`

Chargez les données du fichier de données *Émissions de polluants, CO2 et caractéristiques des véhicules commercialisés en France [2014][^14]* puis répondez aux questions suivantes:

- Quel véhicule est le plus puissant ?
- Quel véhicule est le plus gourmand en ville ?
- Combien y a-t-il de véhicules par type de motorisation (Essence, Diesel, Electrique…) ?
  Faites-en un histogramme.

Représentez le poids et l’émission de CO2 par catégorie de véhicules (colonne *gamme*) et par marque.

Cette représentation doit faire apparaître:

- La moyenne
- La médiane
- La plage de variation des valeurs

Proposez une autre représentation *intéressante* des données disponibles.

# Exercice 3 : sélection par la *méthode* Titanic

Fichier `titanic_selection.ipynb` et `titanic.py`

- Créez une fonction `load_data()` qui ne prend aucun argument et qui retourne le *dataframe* pandas issu du chargement du jeu de données suivant, correspondant à une base de données des naufragés du Titanic <https://web.stanford.edu/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv>.

  Notez que ce chargement doit être fonctionnel sans passer par un fichier intermédiaire sur le *disque dur*.

  Utilisez `head` pour deviner le format de stockage des données.

- Calculez la probabilité conditionnelle qu'une personne survive (`Survived`) en fonction de son genre (`Sex`) et de sa classe de passager (`Pclass`).
- Calculez la probabilité qu'un enfant de 10 ans ou moins en 3ème classe survive.
- Quel est le prix moyen d'un billet en fonction de la classe ? (Pensez à `groupby`)
- Quel est le prix moyen d'un billet en fonction de la classe ? (Pensez à `groupby`)

Les fonctions suivantes doivent toutes prendre (sauf mention contraire) ce *dataframe* comme premier argument sous le nom df. Ex: `calcule_quelque_chose(df, autre_paramètre_de_calcul)`.

- Créez une fonction `get_survivor_count(df)` qui retourne le nombre de survivants sous la forme d’un nombre entier.
- Créez une fonction `get_total_fare(df)` qui retourne la somme des frais (colonne Fare) payés par l’ensemble des passages. Le résultat sera retourné sous la forme d’une chaîne de caractères préfixée par le symbole `$` et avec exactement deux chiffres après la virgule (arrondi au chiffre le plus proche);
  ex: `$1234.56`
- Créez une fonction `get_fare_count(df)` qui retourne le nombre de tarifs différents payés par les passagers (sous la forme d’un entier)
- Créez une fonction `get_survivor_refund(df)` qui retourne le montant qu’il faudrait rembourser si la compagnie devait rembourser tous les survivants (et uniquement les survivants). Le résultat est retourné au même format que get_total_fare.
- Créez une fonction `get_survivor_custom_refund(df)` qui retourne le montant qu’il faudrait rembourser si la compagnie devait rembourser tous les survivants (et uniquement les survivants) suivant la règle:
  - Les voyageurs de première classe sont remboursés cinq fois leur tarif d’embarquement
  - Les voyageurs de second classe sont remboursés trois fois leur tarif d’embarquement
  - Les voyageurs de troisième classe ne sont pas remboursés.

Le résultat est retourné au même format que get_total_fare.

- Créez une fonction `round_percentage(x)` qui prend une valeur x entre 0 et 1 et qui retourne une valeur numérique entre 0.00% et 100.00% avec deux chiffres après la virgule (arrondi à la valeur la plus proche, sans le symbole %)

  /!\ Cette fonction ne prend pas de dataframe en argument.

- Créez une fonction `get_survival_ratio(df)` qui retourne le pourcentage de survie des passagers (sans distinction de classe ou de genre). Le résultat doit être une valeur numérique entre 0.00% et 100.00% avec exactement deux chiffres après la virgule (arrondi à la valeur la plus proche, sans le symbole %).

Cette convention de formattage des pourcentages s’appliquent à toutes les questions suivantes où il est demandé un pourcentage.

- Créez une fonction `get_survival_ratio_by_sex(df)` qui retourne le pourcentage de survie des passagers par genre. Le résultat sera retourné sous la forme d’un tuple composé de deux valeurs a et b où a est la valeur pour les femmes et b celle pour les hommes.
- Créez une fonction `get_survival_ratio_by_sex_and_class(df)` qui retourne pourcentage de survie des passagers par genre et par classe. Le résultat sera retourné sous la forme d’un tableau à deux dimensions de 2 lignes (*female*, *male*) et 3 colonnes (1ère classe, 2nd classe, 3ème classe):

| % de survie des femmes de 1ère classe | % de survie des femmes de 2nde classe | % de survie des femmes de 3ème classe     |
|:--------------------------------------|:--------------------------------------|:------------------------------------------|
| % de survie des hommes de 1ère classe | % de survie des hommes de 2nde classe | % de survie des hommes de 3ème classe     |

- Créez une fonction `get_survival_ratio_for_3rd_child(df)` qui retourne le pourcentage de survie des enfants de strictement moins de 11 ans voyageant en 3ème classe.
- Créez une fonction `get_oldest_survivor(df)` qui retourne sous la forme d’un tableau (1D) composé d’une chaîne de caractères le nom du plus vieux survivant (sans distinction de genre).

<!--
# Exercice X : *Combien de Velib autour de chez moi ?*

Cet exercice vous demande en particulier d'agréger des données issues de sources différentes (faites attention aux formatages des données).

- En utilisant les données Stations Vélib'[^15], définissez la densité de stations vélib par communes (nous considérons chaque arrondissement de Paris comme une commune). Les informations quant à la surface des communes se trouve dans les documents *Répertoire géographique des communes d'Île-de-France*[^16] ainsi que *Arrondissements (de Paris)*[^17]. 
- Calculez la densité de bornes (nombre d’emplacements dans une station) par communes en utilisant les caractéristiques des stations Velib[^18]
-->

# Exercice 4 : Vive l’exercice cyclopédique

Fichier `cyclo.ipynb`

En utilisant les données Citibike[^19] (un seul fichier de données de votre choix), calculez:

- la part des abonnées par rapport aux non-abonnées
- la distance maximale d’un trajet (nous considérons des trajectoires en ligne droite entre le point de départ et celui d’arrivée)
- la répartition des distances parcourues en fonction:
  - du genre
  - de l’heure
- quel vélo a été utilisé le plus grand nombre de fois
- quel vélo a parcouru la plus grande distance

[^14]: <https://www.data.gouv.fr/fr/datasets/emissions-de-co2-et-de-polluants-des-vehicules-commercialises-en-france/>
[^15]: <https://www.data.gouv.fr/fr/datasets/velib-paris-et-communes-limitrophes-idf/>   
[^16]: <https://www.data.gouv.fr/fr/datasets/repertoire-geographique-des-communes-d-ile-de-france-idf/> 
[^17]: <https://www.data.gouv.fr/fr/datasets/arrondissements-1/> 
[^18]: <https://www.data.gouv.fr/fr/datasets/velib-localisation-et-caracteristique-des-stations/> 
[^19]: <https://www.citibikenyc.com/system-data>
