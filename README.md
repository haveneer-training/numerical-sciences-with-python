# Package management avec venv/pip

```shell
# virtualenv
python3 -m virtualenv venv
source ./venv/bin/activate
```

```shell
# install packages
pip3 install -r requirements.txt
```

```shell
# freeze packages
pip3 freeze > requirements.txt
```

# Package management avec conda

```shell
# Install environment
 conda env create --name NumericalSciencesWithPython --file requirements.yml python=3.9
 ```

Les packages suivants ne sont pas supportés par conda 4.11.0; il pourrait manquer quelques fonctionnalités secondaires

* SciencePlots : pour faire un style de graphique plus *sciences*
* ipytest : pour utiliser pytest dans les notebooks jupyter

Les packages suivants ne sont pas supportés par conda mais ne sont que des dépendances implicites dans l'installation
venv/pip; a priori aucun manque de fonctionnalités

* mypy-extensions :
* prometheus-client : idem
* pure-eval : idem
* stack-data : idem

```shell
# Initialize environment
conda activate NumericalSciencesWithPython
```

```shell
# freeze packages
conda env export --name NumericalSciencesWithPython > requirements.yml
```

![](images/conda_on_windows.png)